#
# CSE183 Assignment 8 Backend
# test

openapi: '3.0.3'

info:
  description: CSE183 Assignment 8 Backend
  version: 0.1.0
  title: CSE183 Assignment 8 Backend

servers:
  - url: http://localhost:3010/v0

paths:
  /dummy:
    get:
      description: Dummy Endpoint
      responses:
        200:
          description: Succesful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DummyResponse'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /category:
    get:
      description: Returns all top level categories
      responses:
        200:
          description: Successfully returned all top level categories
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CategoryResponse'
        default:          
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /category/{id}:
    get:
      description: Returns category specified by id along with subcategories
      parameters:
      - name: id
        in: path
        description: id of category
        required: true
        schema:
          type: string
          format: uuid
      responses:
        200:
          description: Successfully returned category specified by id
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/VerboseCategoryResponse'
        404:
          description: Category with specified id not found
        default:          
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /listing:
    get:
      description: Returns all listings
      parameters:
      - name: search
        in: query
        description: Returns all listings related to search query
        required: false
        schema:
          type: string
      - name: userid
        in: query
        description: Returns all listings made by a specified user
        required: false
        schema:
          type: string
          format: uuid
      - name: category
        in: query
        description: Returns all listings that fall under a specified category/subcategory
        required: false
        schema:
          type: string
          format: uuid
      responses:
        200:
          description: Successfully returned all listings
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ListingResponse' 
        default:          
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /listing/{id}:
    get:
      description: Returns one listing specified by its id
      parameters:
      - name: id
        in: path
        description: listing id
        required: true
        schema:
          type: string
          format: uuid
      responses:
        200:
          description: Successfully returned the listing specified by its id
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Listing'
        404:
          description: Specified id does not exist
        default:          
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /user:
    post:
      description: Creates a new user
      requestBody:
        description: User to add
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/NewUser'
      responses:
        201:
          description: User created successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        400:
          description: Submitted User has unexpected properties
        409:
          description: Conflict. User with email already exists
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /user/{id}:
    get:
      description: Returns a specific user specified by id
      parameters:
      - name: id
        in: path
        description: id of user
        required: true
        schema:
          type: string
          format: uuid
      responses:
        200:
          description: Successfully returned specified user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        404:
          description: Specified user with id does not exist
        default:          
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          
  /user/listing:
    post:
      description: Creates a new listing for user
      security:
      - bearerAuth: [] 
      requestBody:
        description: Listing to add
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/NewListing'
      responses:
        201:
          description: User created successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Listing' 
        400:
          description: Submitted Listing has unexpected properties
        404:
          description: Specified user id not found
        409:
          description: Specified category is invalid
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

components:
  schemas:
    Error:
      properties:
        code:
          type: integer
          format: int32
        message:
          type: string
      required:
        - code
        - message

    DummyResponse:
      properties:
        message:
          type: string
      required:
        - message

    VerboseCategoryResponse:
      type: array
      items: 
        type: object
        properties:
          id:
            type: string
            format: uuid
          category:
            type: string
          filters:
            type: array
          subcategories:
            type: array
            items: 
              $ref: '#/components/schemas/Category'

    CategoryResponse:
      type: array
      items:
        $ref: '#/components/schemas/Category'

    Category:
      type: object
      properties:
        id:
          type: string
          format: uuid
        category:
          type: object
          properties:
            name:
              type: string
            associated_filters:
              type: array
              items: 
                type: string
                format: uuid
            icon:
              type: string
          required:
            - name
            - associated_filters
      required:
        - id
        - category

    ListingResponse:
      type: array
      items:
        $ref: '#/components/schemas/Listing'

    Listing:
      type: object
      properties:
        id:
          type: string
          format: uuid
        category_id:
          type: string
          format: uuid
        listing:
          type: object
          properties:
            title:
              type: string
            price:
              type: string
            description:
              type: string
            date_posted:
              type: string
              format: date-time
            location:
              type: object
              properties:
                state: 
                  type: string
                city: 
                  type: string
                country: 
                  type: string
            images:
              type: array
              items: 
                type: string
            replies:
              type: array
              items: 
                $ref: '#/components/schemas/Reply'
          required:
            - title
            - price
            - description
            - date_posted
            - location
            - images
            - replies
      required:
        - id
        - category_id
        - listing

    Reply:
      type: object
      properties:
        userName:
          type: string
        content:
          type: string
        datePosted:
          type: string
          format: date-time
      required:
        - userName
        - content
        - datePosted

    NewUser:
      type: object
      additionalProperties: false
      properties:
        member: 
          type: object
          properties:
            firstName:
              type: string
            lastName:
              type: string
            phoneNumber:
              type: string
              pattern: '\d{10}'
              example: '8056789123'
          required: [firstName, lastName, phoneNumber]
        email:
          type: string
          format: email
          pattern: '(\w+|\d+)[^\s]+@(\w+|\d+)[^\s]*\.((\w+|\d+)[^\s]*){2,6}'
        password:
          type: string
          minLength: 5
      required:
        - member
        - email
        - password

    User:
      type: object
      properties:
        id:
          type: string
          format: uuid
        email:
          type: string
          format: email
        member:
          type: object
          properties:
            firstName:
              type: string
            lastName:
              type: string
            phoneNumber:
              type: string
      required:
        - id
        - email

    NewListing:
      type: object
      properties:
        category_id:
          type: string
          format: uuid
        listing:
          type: array
          items:
            type: object
            additionalProperties: false
            properties:
              title:
                type: string
              price:
                type: number
                format: float
              description:
                type: string
              location:
                type: object
                properties:
                  state: 
                    type: string
                  city: 
                    type: string
                  country: 
                    type: string
              images:
                type: array
                items: 
                  type: string
            required:
              - title
              - price
              - description
              - location
              - images
      required:
        - category_id
        - listing

  securitySchemes:
    bearerAuth:            # arbitrary name for the security scheme
      type: http
      scheme: bearer
      bearerFormat: JWT    # optional, arbitrary value for documentation purposes
