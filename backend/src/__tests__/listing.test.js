const supertest = require('supertest');
const http = require('http');

const db = require('./db');
const app = require('../app');

let server;

beforeAll(() => {
  server = http.createServer(app);
  server.listen();
  request = supertest(server);
  return db.reset();
});

afterAll((done) => {
  server.close(done);
});

test('GET Listing with Non Existent Listing ID', async () => {
  await request.get('/v0/listing/591b428e-1b99-4a56-b653-dab99990b999')
    .expect(404);
});

test('GET Listing with Invalid Listing ID', async () => {
  await request.get('/v0/listing/510-999-9999')
    .expect(400);
});

test('GET Single Listing with Valid Listing ID', async () => {
  const listingID = 'e47327e8-61ab-430b-a852-b1e6a92c03ac';
  await request.get('/v0/listing/' + listingID)
    .expect(200)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.id).toBeDefined();
      expect(data.body.id).toEqual(listingID);
      expect(data.body.category_id).toBeDefined();
      expect(data.body.member_id).toBeDefined();
      expect(data.body.listing).toBeDefined();
      expect(data.body.listing.price).toBeDefined();
      expect(data.body.listing.title).toBeDefined();
      expect(data.body.listing.images).toBeDefined();
      expect(data.body.listing.replies).toBeDefined();
      expect(data.body.listing.location).toBeDefined();
      expect(data.body.listing.date_posted).toBeDefined();
      expect(data.body.listing.description).toBeDefined();
    });
});

test('GET Listings for Non Existent User ID', async () => {
  await request.get('/v0/listing?userid=591b428e-1b99-4a56-b653-dab99990b999')
    .expect(404);
});

test('GET Listings for Invalid User ID', async () => {
  await request.get('/v0/listing?userid=510-999-9999')
    .expect(400);
});

test('GET Listings for User with Valid ID', async () => {
  const userID = '3cd6c9dc-a941-4702-8476-16c71c6244f1';
  await request.get('/v0/listing?userid=' + userID)
    .expect(200)
    .expect('Content-Type', /json/)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toEqual(1);
      expect(data.body[0].id).toBeDefined();
      expect(data.body[0].category_id).toBeDefined();
      expect(data.body[0].member_id).toBeDefined();
      expect(data.body[0].member_id).toEqual(userID);
      expect(data.body[0].listing).toBeDefined();
      expect(data.body[0].listing.price).toBeDefined();
      expect(data.body[0].listing.title).toBeDefined();
      expect(data.body[0].listing.images).toBeDefined();
      expect(data.body[0].listing.replies).toBeDefined();
      expect(data.body[0].listing.location).toBeDefined();
      expect(data.body[0].listing.date_posted).toBeDefined();
      expect(data.body[0].listing.description).toBeDefined();
    });
});

test('GET Listings for User with Valid ID But No Listings', async () => {
  await request.get('/v0/listing?userid=522fce85-3aa2-43ec-b997-69093143478f')
    .expect(200)
    .expect('Content-Type', /json/)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toEqual(0);
    });
});

test('GET Listings for User with multiple listings', async () => {
  await request.get('/v0/listing?userid=7b917fe4-a164-414c-a80b-1b84bb2a4dd3')
    .expect(200)
    .expect('Content-Type', /json/)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toEqual(2);
    });
});

test('GET All Listings', async () => {
  await request.get('/v0/listing')
    .expect(200)
    .expect('Content-Type', /json/)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toBeGreaterThan(10);
    });
});

test('GET Listings for Non Existent Category Id', async () => {
  await request.get('/v0/listing?category=591b428e-1b99-4a56-b653-dab99990b999')
    .expect(404);
});

test('GET Listings for Non Existent Category Id', async () => {
  await request.get('/v0/listing?category=99-9898jjbj-989')
    .expect(400);
});

test('GET Listings for Valid Parent Category Id', async () => {
  await request.get('/v0/listing?category=3fc8800d-56bd-41bb-b103-026c0e1453da')
    .expect(200)
    .expect('Content-Type', /json/)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toEqual(7);
    });
});

test('GET Listings for Valid Subcategory Id', async () => {
  const subcategoryId = 'ed5a6dc4-eabf-4412-83ba-bdacb37de451';
  await request.get('/v0/listing?category=' + subcategoryId)
    .expect(200)
    .expect('Content-Type', /json/)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toEqual(3);
      expect(data.body[0].category_id).toBeDefined();
      expect(data.body[0].category_id).toEqual(subcategoryId);
    });
});

test('GET Listings for Category with Valid ID But No Listings', async () => {
  await request.get('/v0/listing?category=266d8561-7010-422b-bb31-e213160e36c5')
    .expect(200)
    .expect('Content-Type', /json/)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toEqual(0);
    });
});

test('GET Listings Related to Search Query', async () => {
  await request.get('/v0/listing?search=Ford')
    .expect(200)
    .expect('Content-Type', /json/)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toEqual(3);
    });
});

test('GET Specific Single Listing Related to Search Query', async () => {
  await request.get('/v0/listing?search=Subar')
    .expect(200)
    .expect('Content-Type', /json/)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toEqual(1);
    });
});

test('GET Listings Related to Search Query With No Matches', async () => {
  await request.get('/v0/listing?search=ZZhgrirfhi')
    .expect(200)
    .expect('Content-Type', /json/)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toEqual(0);
    });
});


