const supertest = require('supertest');
const http = require('http');

const db = require('./db');
const app = require('../app');

let server;

beforeAll(() => {
  server = http.createServer(app);
  server.listen();
  request = supertest(server);
  return db.reset();
});

afterAll((done) => {
  server.close(done);
});

test('GET All Parent Categories', async () => {
  await request.get('/v0/category')
    .expect(200)
    .expect('Content-Type', /json/)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toEqual(15);
    });
});

test('GET Category with Non Existent ID', async () => {
  await request.get('/v0/category/591b428e-1b99-4a56-b653-dab99990b999')
    .expect(404);
});

test('GET Category with Invalid ID', async () => {
  await request.get('/v0/category/510-999-9999')
    .expect(400);
});

test('GET Category with Valid ID and No Subcategories', async () => {
  const categoryId = 'eb386d21-1805-4828-b672-686d1e44b7dd';
  await request.get('/v0/category/' + categoryId)
    .expect(200)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toEqual(1);
      expect(data.body[0].id).toBeDefined();
      expect(data.body[0].id).toEqual(categoryId);
      expect(data.body[0].category).toBeDefined();
      expect(data.body[0].category).toEqual('Apparel');
      expect(data.body[0].filters).toBeDefined();
      expect(data.body[0].subcategories).toBeDefined();
      expect(data.body[0].subcategories.length).toEqual(0);
    });
});

test('GET Category with Valid ID and With Subcategories', async () => {
  const categoryId = '3fc8800d-56bd-41bb-b103-026c0e1453da';
  await request.get('/v0/category/' + categoryId)
    .expect(200)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.length).toEqual(1);
      expect(data.body[0].id).toBeDefined();
      expect(data.body[0].id).toEqual(categoryId);
      expect(data.body[0].category).toBeDefined();
      expect(data.body[0].category).toEqual('Vehicles');
      expect(data.body[0].filters).toBeDefined();
      expect(data.body[0].subcategories).toBeDefined();
      expect(data.body[0].subcategories.length).toEqual(2);
    });
});
