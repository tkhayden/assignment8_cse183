const supertest = require('supertest');
const http = require('http');

const db = require('./db');
const app = require('../app');

let server;

beforeAll(() => {
  server = http.createServer(app);
  server.listen();
  request = supertest(server);
  return db.reset();
});

afterAll((done) => {
  server.close(done);
});

describe('Test user-creation', () => {
  test('Invalid email format', async () => {
    const badEmails = ['cse183-studentucsc.com',
      'student@ucscedu', 'cse183-student@ucsc.',
      'cse183-student@.net', 'cse183-student@.', '@ucsc.edu'];
    const info = {'member': {
      'firstName': 'Billy',
      'lastName': 'Bob',
      'phoneNumber': '4759440682',
    },
    'email': '',
    'password': 'password1',
    };
    for (email of badEmails) {
      info.email = email;
      await request.post('/v0/user').send(info)
        .expect(400);
    }
  });
  test('Missing a password', async () => {
    const info = {'member': {
      'firstName': 'Billy',
      'lastName': 'Bob',
      'phoneNumber': '4759440682',
    },
    'email': 'bgallanders0@youtube.com',
    };
    await request.post('/v0/user').send(info)
      .expect(400);
  });
  test('Invalid phone number', async () => {
    const info = {'member': {
      'firstName': 'Billy',
      'lastName': 'Bob',
      'phoneNumber': '47540682',
    },
    'email': 'bgallanders0@youtube.com',
    };
    await request.post('/v0/user').send(info)
      .expect(400);
  });
  test('Missing first name', async () => {
    const info = {'member': {
      'lastName': 'Bob',
      'phoneNumber': '4759440682',
    },
    'email': 'testmail12@test.com',
    'password': 'password1',
    };

    await request.post('/v0/user').send(info)
      .expect(400);
  });
  test('Extra properties', async () => {
    const info = {'member': {
      'firstName': 'Billy',
      'lastName': 'Bob',
      'phoneNumber': '4759440682',
    },
    'email': 'testil12@test.com',
    'password': 'password1',
    'username': 'invalid',
    };

    await request.post('/v0/user').send(info)
      .expect(400);
  });
  test('Email is already in use', async () => {
    const info = {'member': {
      'firstName': 'Billy',
      'lastName': 'Bob',
      'phoneNumber': '4759440682',
    },
    'email': 'bgallanders0@youtube.com',
    'password': 'password1',
    };
    await request.post('/v0/user').send(info)
      .expect(409);
  });
  test('Password length < 5', async () => {
    const info = {'member': {
      'firstName': 'Billy',
      'lastName': 'Bob',
      'phoneNumber': '4723440682',
    },
    'email': 'testmail12@test.com',
    'password': 'pass',
    };
    await request.post('/v0/user').send(info)
      .expect(400);
  });
  test('New valid account. 201 status code.', async () => {
    const info = {'member': {
      'firstName': 'Billy',
      'lastName': 'Bob',
      'phoneNumber': '4723440682',
    },
    'email': 'testmail12@test.com',
    'password': 'password1',
    };
    await request.post('/v0/user').send(info)
      .expect(201);
  });
  test('New valid account. 201 status code.', async () => {
    const info = {'member': {
      'firstName': 'Billy',
      'lastName': 'Bob',
      'phoneNumber': '4759210682',
    },
    'email': 'testmail1234@test.com',
    'password': 'password1',
    };
    const response =await request.post('/v0/user').send(info);
    expect(response.body.email).toBe('testmail1234@test.com');
    expect(response.body.id).toBeDefined();
  });
});
describe('User login', () => {
  test('Missing email property', async () => {
    const invalidInfo = {
      'password': 'password1',
    };
    await request.post('/login').send(invalidInfo)
      .expect(400);
  });
  test('Missing password property', async () => {
    const invalidInfo = {
      'email': 'invalid1232@bad.com',
    };
    await request.post('/login').send(invalidInfo)
      .expect(400);
  });
  test('Invalid email address', async () => {
    const invalidInfo = {
      'email': 'invalid1232@bad.com',
      'password': 'password1',
    };
    await request.post('/login').send(invalidInfo)
      .expect(401);
  });

  test('Incorrect password', async () => {
    const badPass = ['passwor1', 'password', 'passord1', 'password11'];
    const info= {
      'email': 'testmail1234@test.com',
      'password': '',
    };
    for (password of badPass) {
      info.password = password;
      await request.post('/login').send(info)
        .expect(401);
    }
  });
  test('Valid Login. Status code 200', async () => {
    const info = {
      'email': 'testmail1234@test.com',
      'password': 'password1',
    };
    await request.post('/login').send(info)
      .expect(200);
  });
  test('Valid Login response info.', async () => {
    const info = {'member': {
      'firstName': 'Billy',
      'lastName': 'Bob',
      'phoneNumber': '4759210682',
    },
    'email': 'testmail34@test.com',
    'password': 'password1',
    };
    const response = await request.post('/v0/user').send(info);
    const id = response.body.id;
    const login = {
      'email': response.body.email,
      'password': 'password1',
    };
    const loginResponse = await request.post('/login').send(login);
    expect(loginResponse.body.email).toBe(login.email);
    expect(loginResponse.body.id).toBe(id);
    expect(loginResponse.body.accessToken).toBeDefined();
  });
});


test('GET User with Non Existent ID', async () => {
  await request.get('/v0/user/591b428e-1b99-4a56-b653-dab99990b999')
    .expect(404);
});

test('GET User with Invalid ID', async () => {
  await request.get('/v0/user/510-999-9999')
    .expect(400);
});

test('GET User with Valid ID', async () => {
  const userId = '523a986c-5a92-4004-9a8d-2d05b609dc81';
  await request.get('/v0/user/' + userId)
    .expect(200)
    .then((data) => {
      expect(data).toBeDefined();
      expect(data.body).toBeDefined();
      expect(data.body.id).toBeDefined();
      expect(data.body.id).toEqual(userId);
      expect(data.body.email).toBeDefined();
      expect(data.body.member).toBeDefined();
      expect(data.body.member.firstName).toBeDefined();
      expect(data.body.member.lastName).toBeDefined();
      expect(data.body.member.phoneNumber).toBeDefined();
    });
});
