const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const secret = require('./data/secret.json');
const db = require('./db');


exports.createUser = async (req, res) => {
  const info = req.body;
  const saltRounds = 10;

  if ( await db.userExists(info.email, false)) {
    res.status(409).end();
  } else {
    const passwordHash = await bcrypt.hash(info.password, saltRounds);
    info.password = passwordHash;
    const newUser = await db.addUser(info);
    const response = {'id': newUser[0].id, 'email': newUser[0].email};
    res.status(201).json(response);
  }
};

exports.authenticate = async (req, res) => {
  const {email, password} = req.body;
  if ((email === undefined )|| (password === undefined)) {
    res.status(400).end();
    return;
  }
  const user = await db.userExists(email, false);
  if (user) {
    const passwordCorrect = bcrypt.compareSync(password, user[0].password_hash);
    if (!passwordCorrect) {
      res.status(401).end();
      return;
    } else {
      const accessToken = jwt.sign(
        {email: user[0].email,
          id: user[0].id,
        },
        secret.accessToken, {
          expiresIn: '60m',
          algorithm: 'HS256',
        });
      const response = {'id': user[0].id, 'email': user[0].email,
        'accessToken': accessToken};
      res.status(200).json(response);
    }
  } else {
    res.status(401).end();
  }
};

exports.getUserById = async (req, res) => {
  const user = await db.selectUserById(req.params.id);
  if (user) {
    delete user.password_hash;
    res.status(200).json(user);
  } else {
    res.status(404).send();
  }
};

