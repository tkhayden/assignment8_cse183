const db = require('./db');

exports.getListings = async (req, res) => {
  const userId = req.query.userid;
  const categoryId = req.query.category;
  const search = req.query.search;
  let listings;

  if (userId) {
    listings = await db.selectListings(userId, undefined);
  } else if (categoryId) {
    listings = await db.selectListingsByCategory(categoryId);
  } else if (search) {
    listings = await db.selectListings(undefined, search);
  } else {
    listings = await db.selectListings();
  }

  if (listings) {
    res.status(200).json(listings);
  } else {
    res.status(404).send();
  }
};

exports.getListingById = async (req, res) => {
  const listing = await db.selectListingById(req.params.id);
  if (listing) {
    res.status(200).json(listing);
  } else {
    res.status(404).send();
  }
};
