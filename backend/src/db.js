const {Pool} = require('pg');

const pool = new Pool({
  host: 'localhost',
  port: 5432,
  database: process.env.POSTGRES_DB,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
});

const categoryExists = async (categoryId) => {
  const select = 'SELECT * FROM category WHERE id = $1';
  const query = {
    text: select,
    values: [categoryId],
  };

  const {rows} = await pool.query(query);
  return rows.length == 0 ? false : rows;
};

exports.userExists = async (userInfo, byID) => {
  let select = 'SELECT * FROM member';
  if (byID) {
    select += ' WHERE id = $1';
  } else {
    select += ' WHERE email = $1';
  }
  const query = {
    text: select,
    values: [userInfo],
  };

  const {rows} = await pool.query(query);
  return rows.length == 0 ? false : rows;
};

exports.addUser = async (newUser) => {
  const select = 'INSERT INTO member(member, email, password_hash)'+
   'VALUES ($1, $2, $3) RETURNING id, email';

  const query = {
    text: select,
    values: [newUser.member, newUser.email, newUser.password],
  };
  const {rows} = await pool.query(query);
  return rows;
};

exports.selectUserById = async (id) => {
  const select = 'SELECT * FROM member WHERE id = $1';
  const query = {
    text: select,
    values: [id],
  };

  const {rows} = await pool.query(query);
  return rows.length == 1 ? rows[0] : undefined;
};

exports.selectListingsByCategory = async (categoryId) => {
  if (!(await categoryExists(categoryId))) {
    return undefined;
  }

  const subcategorySelect = 'SELECT * FROM category WHERE id = $1';
  const query = {
    text: subcategorySelect,
    values: [categoryId],
  };
  const {rows} = await pool.query(query);
  const subCategories = [categoryId];
  for (const subcategoryId of rows[0].category.subcategories) {
    subCategories.push(subcategoryId);
  }

  const allListings = [];
  const select = 'SELECT * FROM listing WHERE category_id = $1';
  for (const subCategory of subCategories) {
    const query = {
      text: select,
      values: [subCategory],
    };
    const {rows} = await pool.query(query);
    for (const row of rows) {
      allListings.push(row);
    }
  }
  return allListings;
};

exports.selectListings = async (userId, search) => {
  let select = 'SELECT * FROM listing';
  let queryValues = [];
  if (userId) {
    if (!(await exports.userExists(userId, true))) {
      return undefined;
    }
    select += ' WHERE member_id = $1';
    queryValues = [userId];
  } else if (search) {
    select += ' WHERE listing ->> \'title\' ILIKE $1';
    select += ' OR listing ->> \'description\' ILIKE $2';
    queryValues = [`%${search}%`, `%${search}%`];
  }

  const query = {
    text: select,
    values: queryValues,
  };

  const {rows} = await pool.query(query);
  return rows;
};

exports.selectListingById = async (id) => {
  const select = 'SELECT * FROM listing WHERE id = $1';
  const query = {
    text: select,
    values: [id],
  };

  const {rows} = await pool.query(query);
  return rows.length == 1 ? rows[0] : undefined;
};

exports.selectParentCategories = async () => {
  const select = 'SELECT * FROM category WHERE parent_id IS NULL';
  const query = {
    text: select,
    values: [],
  };

  const {rows} = await pool.query(query);
  return rows;
};

exports.selectCategoryById = async (categoryId) => {
  const select = 'SELECT * FROM category WHERE id = $1';
  const query = {
    text: select,
    values: [categoryId],
  };

  const {rows} = await pool.query(query);
  return rows.length == 1 ? rows[0] : undefined;
};
