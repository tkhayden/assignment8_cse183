const express = require('express');
const cors = require('cors');
const yaml = require('js-yaml');
const swaggerUi = require('swagger-ui-express');
const fs = require('fs');
const path = require('path');
const OpenApiValidator = require('express-openapi-validator');

const dummy = require('./dummy');
const listing = require('./listing');
const user = require('./user');
const category = require('./category');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));


const apiSpec = path.join(__dirname, '../api/openapi.yaml');
const apidoc = yaml.load(fs.readFileSync(apiSpec, 'utf8'));
app.use('/v0/api-docs', swaggerUi.serve, swaggerUi.setup(apidoc));
app.post('/login', user.authenticate);
app.post('/register', user.createUser);

app.use(
  OpenApiValidator.middleware({
    apiSpec: apiSpec,
    validateRequests: true,
    validateResponses: true,
  }),
);

app.get('/v0/dummy', dummy.get);
app.get('/v0/listing', listing.getListings);
app.get('/v0/category', category.getParentCategories);
app.get('/v0/category/:id', category.getCategoryById);
app.get('/v0/listing/:id', listing.getListingById);
app.get('/v0/user/:id', user.getUserById);
app.post('/v0/user', user.createUser);

app.use((err, req, res, next) => {
  res.status(err.status).json({
    message: err.message,
    errors: err.errors,
    status: err.status,
  });
});

module.exports = app;
