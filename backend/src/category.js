const db = require('./db');

exports.getParentCategories = async (req, res) => {
  const categories = await db.selectParentCategories();
  // eslint-disable-next-line camelcase
  const strippedCategories = categories.map(({parent_id, ...rest}) => rest);
  res.status(200).json(strippedCategories);
};

exports.getCategoryById = async (req, res) => {
  const parentCategory = await db.selectCategoryById(req.params.id);
  if (parentCategory == undefined) {
    res.status(404).send();
    return;
  }

  const allSubCategories = [];
  const subcategoryIds = parentCategory.category.subcategories;
  for (const subcategoryId of subcategoryIds) {
    const subcategory = await db.selectCategoryById(subcategoryId);
    delete subcategory.parent_id;
    delete subcategory.category.subcategories;
    allSubCategories.push(subcategory);
  }

  const response = {
    id: parentCategory.id,
    category: parentCategory.category.name,
    filters: parentCategory.category.associated_filters,
    subcategories: allSubCategories,
  };

  res.status(200).json([response]);
};

