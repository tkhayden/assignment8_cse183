-- Dummy table --
DROP TABLE IF EXISTS dummy;
CREATE TABLE dummy(created TIMESTAMP WITH TIME ZONE);

-- Your database schema goes here --
DROP TABLE IF EXISTS replies;
DROP TABLE IF EXISTS listing;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS member;

CREATE TABLE member ( id UUID UNIQUE PRIMARY KEY DEFAULT gen_random_uuid(), member jsonb, email VARCHAR(60) UNIQUE NOT NULL, password_hash TEXT NOT NULL );

CREATE TABLE category ( id UUID UNIQUE PRIMARY KEY DEFAULT gen_random_uuid(), parent_id UUID, category jsonb, FOREIGN KEY (parent_id) REFERENCES category(id) );

CREATE TABLE listing ( id UUID UNIQUE PRIMARY KEY DEFAULT gen_random_uuid(), category_id UUID NOT NULL, member_id UUID NOT NULL, listing jsonb, FOREIGN KEY (category_id) REFERENCES category(id), FOREIGN KEY (member_id) REFERENCES member(id) );

CREATE TABLE replies ( id UUID UNIQUE PRIMARY KEY DEFAULT gen_random_uuid(), reply jsonb, member_id UUID NOT NULL, listing_id UUID NOT NULL, FOREIGN KEY (member_id) REFERENCES member(id), FOREIGN KEY (listing_id) REFERENCES listing(id) );

-- CREATE TABLE filters (
--    id UUID UNIQUE PRIMARY KEY DEFAULT gen_random_uuid(),
--    filter_name VARCHAR(30),
--    filter_type VARCHAR(30)
-- );

-- Example INSERT INTO member table
-- INSERT INTO member (member, email, password) VALUES (
--    '{"firstName": "John", "lastName": "Doe"....}'
--   'johndoe@mail.com',
--   crypt('johnspassword', gen_salt('bf'))
-- );
