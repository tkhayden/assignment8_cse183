# Assignment8_CSE183

Group 21's Assignment 8 for CSE 183. A Facebook Marketplace clone as a Single Page Full Stack Web App using the NERP Stack.


## **Setup**

- Install [npm](https://nodejs.org/en/download/)
- Install [Docker](https://www.docker.com/products/docker-desktop/)
- Clone the repository: `git clone https://gitlab.com/tkhayden/assignment8_cse183.git`
- Install dependencies
    1. In the frontend folder, type `npm install`
    2. In the backend folder, type `npm install`
- Build PostgreSQL image and run the container
    1. In the backend folder, type `docker compose up`
- Run the Node server
    1. In the backend folder, type `npm start`
- Run the React front end
    1. In the frontend folder, type `npm start`
