const puppeteer = require('puppeteer');
const http = require('http');
const path = require('path');
const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');

require('dotenv').config();
const app = require('../../backend/src/app');

let backend;
let frontend;
let browser;
let page;

beforeAll(() => {
  backend = http.createServer(app);
  backend.listen(3010, () => {
    console.log('Backend Running at http://localhost:3010');
  });
  frontend = http.createServer(
    express()
      .use('/v0', createProxyMiddleware({ 
        target: 'http://localhost:3010/',
        changeOrigin: true}))
      .use('/static', express.static(
        path.join(__dirname, '..', '..', 'frontend', 'build', 'static')))
      .get('*', function(req, res) {
        res.sendFile('index.html', 
            {root:  path.join(__dirname, '..', '..', 'frontend', 'build')})
      })
  );
  frontend.listen(3000, () => {
    console.log('Frontend Running at http://localhost:3000');
  });
});

afterAll((done) => {
  backend.close(() => { 
    frontend.close(done);
  });
});

beforeEach(async () => {
  browser = await puppeteer.launch({
    args: [
      '--no-sandbox',
      '--headless',
    ],
  });
  page = await browser.newPage();
});

afterEach(async () => {
  await browser.close();
});

test('Click on Category in Desktop View', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 1100, 'height': 900 })
  await page.waitForTimeout(1000);
  await page.click('[id="3fc8800d-56bd-41bb-b103-026c0e1453da"]');


  await page.waitForTimeout(1000);
  const trucksSubCtgry = await page.$('#a4383321-84ff-4b6e-8f57-86fd4ae7d9b9');
  const cont = await (await trucksSubCtgry.getProperty('textContent'))
    .jsonValue()
  expect(cont).toBe('Trucks');
}, 10000);

test('Click on Subcategory in Desktop View', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 1100, 'height': 900 })
  await page.waitForTimeout(1000);
  await page.click('[id="3fc8800d-56bd-41bb-b103-026c0e1453da"]');

  await page.waitForTimeout(1000);
  await page.click('#a4383321-84ff-4b6e-8f57-86fd4ae7d9b9');

  await page.waitForTimeout(1000);
  const notTruckListing = await page.$('#c6787a4a-4452-464c-8b9c-d851a0a1285e');
  expect(notTruckListing).toBeNull();

  const truckListing = await page.$('#e47327e8-61ab-430b-a852-b1e6a92c03ac');
  expect(truckListing).not.toBeNull();
}, 10000);

test('Click on Category With No Subcategory in Desktop View', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 1100, 'height': 900 })
  await page.waitForTimeout(1000);
  await page.click('[id="1ec5a85b-0c85-4a55-8386-2894f769a0f3"]');

  await page.waitForTimeout(1000);
  const subCatBar = await page.$('aria/desktop sub-category bar');
  expect(subCatBar).toBeNull();
}, 10000);

test('Click on Category With No Listings in Desktop View', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 1100, 'height': 900 })
  await page.waitForTimeout(1000);
  await page.click('[id="1ec5a85b-0c85-4a55-8386-2894f769a0f3"]');

  await page.waitForTimeout(1000);
  const relatedListing = await page.$('aria/listing');
  expect(relatedListing).toBeNull();
}, 10000);

test('Click on Category in Mobile View', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 700, 'height': 600 })
  await page.waitForTimeout(1000);
  await page.click('aria/All Categories');

  await page.waitForTimeout(1000);
  await page.click('[id="3fc8800d-56bd-41bb-b103-026c0e1453da"]');

  await page.waitForTimeout(1000);
  const vehiclesCategoryLabel = await page.$('aria/Vehicles');
  const cont = await (await vehiclesCategoryLabel.getProperty('textContent'))
    .jsonValue()
  expect(cont).toBe('Vehicles');
}, 10000);

test('Click on Category Then Subactegory Mobile View', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 700, 'height': 600 })
  await page.waitForTimeout(1000);
  await page.click('aria/All Categories');

  await page.waitForTimeout(1000);
  await page.click('[id="3fc8800d-56bd-41bb-b103-026c0e1453da"]');

  await page.waitForTimeout(1000);
  await page.click('#a4383321-84ff-4b6e-8f57-86fd4ae7d9b9');

  await page.waitForTimeout(1000);
  const trucksCategoryLabel = await page.$('aria/Trucks');
  const cont = await (await trucksCategoryLabel.getProperty('textContent'))
    .jsonValue()
  expect(cont).toBe('Trucks');
}, 10000);

