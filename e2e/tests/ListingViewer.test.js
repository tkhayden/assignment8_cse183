const puppeteer = require('puppeteer');
const http = require('http');
const path = require('path');
const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');

require('dotenv').config();
const app = require('../../backend/src/app');

let backend;
let frontend;
let browser;
let page;

beforeAll(() => {
  backend = http.createServer(app);
  backend.listen(3010, () => {
    console.log('Backend Running at http://localhost:3010');
  });
  frontend = http.createServer(
    express()
      .use('/v0', createProxyMiddleware({ 
        target: 'http://localhost:3010/',
        changeOrigin: true}))
      .use('/static', express.static(
        path.join(__dirname, '..', '..', 'frontend', 'build', 'static')))
      .get('*', function(req, res) {
        res.sendFile('index.html', 
            {root:  path.join(__dirname, '..', '..', 'frontend', 'build')})
      })
  );
  frontend.listen(3000, () => {
    console.log('Frontend Running at http://localhost:3000');
  });
});

afterAll((done) => {
  backend.close(() => { 
    frontend.close(done);
  });
});

beforeEach(async () => {
  browser = await puppeteer.launch({
    args: [
      '--no-sandbox',
      '--headless',
    ],
  });
  page = await browser.newPage();
});

afterEach(async () => {
  await browser.close();
});

test('Open Listing Viewer Then Close it', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 1000, 'height': 800 })
  await page.waitForTimeout(1000);
  await page.click('#e47327e8-61ab-430b-a852-b1e6a92c03ac');

  await page.$('aria/Close Viewer')
  await page.click('aria/Close Viewer');
  const closeViewerBtn = await page.$('aria/Close Viewer');
  expect(closeViewerBtn).toBeNull();
}, 10000);

test('Open Listing Viewer Then Click on Next Image', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 1000, 'height': 800 })
  await page.waitForTimeout(1000);
  await page.click('#e47327e8-61ab-430b-a852-b1e6a92c03ac');

  await page.$('aria/Go to next page')
  await page.click('aria/Go to next page')
  await page.waitForTimeout(1000);
  await page.$('aria/page 2')
  const currentPage = await page.$('aria/page 2');
  const cont = await (await currentPage.getProperty('textContent')).jsonValue()
  expect(cont).toBe('2');
}, 10000);

test('Open Listing Viewer Then Click on Unauthed Login Button', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 1000, 'height': 800 })
  await page.waitForTimeout(1000);
  await page.click('#e47327e8-61ab-430b-a852-b1e6a92c03ac');

  await page.$('aria/Unauthed Viewer Login')
  await page.click('aria/Unauthed Viewer Login')
  await page.waitForTimeout(1000);

  const signInBtn = await page.$('aria/Login Button');
  const cont = await (await signInBtn.getProperty('textContent')).jsonValue()
  expect(cont).toBe('Sign In');
}, 10000);

