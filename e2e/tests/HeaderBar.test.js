const puppeteer = require('puppeteer');
const http = require('http');
const path = require('path');
const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');

require('dotenv').config();
const app = require('../../backend/src/app');

let backend;
let frontend;
let browser;
let page;

beforeAll(() => {
  backend = http.createServer(app);
  backend.listen(3010, () => {
    console.log('Backend Running at http://localhost:3010');
  });
  frontend = http.createServer(
    express()
      .use('/v0', createProxyMiddleware({ 
        target: 'http://localhost:3010/',
        changeOrigin: true}))
      .use('/static', express.static(
        path.join(__dirname, '..', '..', 'frontend', 'build', 'static')))
      .get('*', function(req, res) {
        res.sendFile('index.html', 
            {root:  path.join(__dirname, '..', '..', 'frontend', 'build')})
      })
  );
  frontend.listen(3000, () => {
    console.log('Frontend Running at http://localhost:3000');
  });
});

afterAll((done) => {
  backend.close(() => { 
    frontend.close(done);
  });
});

beforeEach(async () => {
  browser = await puppeteer.launch({
    args: [
      '--no-sandbox',
      '--headless',
    ],
  });
  page = await browser.newPage();
});

afterEach(async () => {
  await browser.close();
});

test('Press Login Button in Mobile View Redirects to Login Page', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 700, 'height': 600 })
  await page.waitForTimeout(1000);
  await page.click('aria/log-in button');

  await page.waitForTimeout(1000);
  const signInBtn = await page.$('aria/Login Button');
  const cont = await (await signInBtn.getProperty('textContent')).jsonValue()
  expect(cont).toBe('Sign In');
}, 10000);

test('Quick Login Through Headerbar Desktop View', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 1100, 'height': 900 })
  await page.waitForTimeout(1000);
  await page.type('#filled', 'bgallanders0@youtube.com');
  await page.type('#filled-basic', 'password');
  await page.click('aria/log-in submit button');
}, 10000);
