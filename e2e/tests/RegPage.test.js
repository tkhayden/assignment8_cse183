const puppeteer = require('puppeteer');
const http = require('http');
const path = require('path');
const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');

require('dotenv').config();
const app = require('../../backend/src/app');

let backend;
let frontend;
let browser;
let page;

beforeAll(() => {
  backend = http.createServer(app);
  backend.listen(3010, () => {
    console.log('Backend Running at http://localhost:3010');
  });
  frontend = http.createServer(
    express()
      .use('/v0', createProxyMiddleware({ 
        target: 'http://localhost:3010/',
        changeOrigin: true}))
      .use('/static', express.static(
        path.join(__dirname, '..', '..', 'frontend', 'build', 'static')))
      .get('*', function(req, res) {
        res.sendFile('index.html', 
            {root:  path.join(__dirname, '..', '..', 'frontend', 'build')})
      })
  );
  frontend.listen(3000, () => {
    console.log('Frontend Running at http://localhost:3000');
  });
});

afterAll((done) => {
  backend.close(() => { 
    frontend.close(done);
  });
});

beforeEach(async () => {
  browser = await puppeteer.launch({
    args: [
      '--no-sandbox',
      '--headless',
    ],
  });
  page = await browser.newPage();
});

afterEach(async () => {
  await browser.close();
});

test('Invalid Sign Up with Already Existing Email', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 700, 'height': 900 })
  await page.waitForTimeout(1000);
  await page.click('aria/log-in button');
  await page.click('#sign-up');

  await page.waitForTimeout(1000);
  await page.type('#email', 'bgallanders0@youtube.com');
  await page.type('#password', 'password');
  await page.click('aria/Sign Up Button');

  await page.waitForTimeout(1000);
  const errMsg = await page.$('#invalid-signup');
  const cont = await (await errMsg.getProperty('textContent'))
    .jsonValue();
  expect(cont).toContain('User with submitted Email already exists.');
}, 10000);

test('Signing Up', async () => {
  await page.goto('http://localhost:3000');
  await page.setViewport({'width': 700, 'height': 900 })
  await page.waitForTimeout(1000);
  await page.click('aria/log-in button');

  await page.waitForTimeout(1000);
  await page.click('#sign-up');

  await page.waitForTimeout(1000);
  await page.type('#email', 'new@youtube.com');
  await page.type('#password', 'password');
  await page.type('#firstName', 'testFirst');
  await page.type('#lastName', 'testLast');
  await page.click('aria/Sign Up Button');
}, 10000);
