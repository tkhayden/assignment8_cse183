import React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import {useUserUpdate} from './providers/UserProvider';
import {useHistory} from 'react-router-dom';

/**
 *
 * @param {*} props
 * @return {object}
 */
function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        GroupTwentyOne
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const theme = createTheme();

/**
 * Credit to https://github.com/mui-org/material-ui/
 * blob/master/docs/src/pages/getting-started/templates/
 * sign-up/SignUp.js
 * @return {object}
 */
export default function RegPage() {
  const history = useHistory();
  const updateUser = useUserUpdate();
  const [unsucessfulRegister, setUnsucessfulRegister] = React.useState(false);
  const [unsucessfulLogin, setUnsucessfulLogin] = React.useState(false);

  const authenticateNewUser = async (email, password) => {
    const userInfo = {email: email, password: password};
    try {
      const response = await fetch('/login', {
        method: 'POST',
        body: JSON.stringify(userInfo),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const info = await response.json();
      localStorage.setItem('user', JSON.stringify(info));
      updateUser(info);
      setUnsucessfulLogin(false);
      history.push('/');
    } catch (exception) {
      setUnsucessfulLogin(true);
      history.push('/login');
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const newUserInfo = {
      email: data.get('email'),
      password: data.get('password'),
      member: {
        firstName: data.get('firstName'),
        lastName: data.get('lastName'),
        phoneNumber: '',
      },
    };
    try {
      const response = await fetch('/register', {
        method: 'POST',
        body: JSON.stringify(newUserInfo),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const info = await response.json();
      await authenticateNewUser(info.email, data.get('password'));
    } catch (exception) {
      setUnsucessfulRegister(true);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{m: 1, bgcolor: 'secondary.main'}}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          { unsucessfulLogin &&
            <>
              <Typography
                sx={{color: 'red', mt: 1}}
                component="h1"
                variant="body2"
              >
                Unexpected Error Occured.
              </Typography>
              <Typography
                sx={{color: 'red', mt: 1}}
                component="h1"
                variant="body2"
              >
                Please Try Logging Using your new Credentials.
              </Typography>
            </>
          }
          { unsucessfulRegister &&
              <Typography
                id="invalid-signup"
                sx={{color: 'red', mt: 1}}
                component="h1"
                variant="body2"
              >
                User with submitted Email already exists.
                Try Logging in.
              </Typography>
          }
          <Box component="form" noValidate onSubmit={handleSubmit} sx={{mt: 3}}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="given-name"
                  name="firstName"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  aria-label="First Name"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  aria-label="Last Name"
                  autoComplete="family-name"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  aria-label="Email Address"
                  autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  aria-label="Password"
                  autoComplete="new-password"
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              aria-label="Sign Up Button"
              sx={{mt: 3, mb: 2}}
            >
              Sign Up
            </Button>
            <Grid container justifyContent="center">
              <Grid item>
                <Link href="/login" variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{mt: 5}} />
      </Container>
    </ThemeProvider>
  );
}
