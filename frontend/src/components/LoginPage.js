import React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import {useUserUpdate} from './providers/UserProvider';
import {useHistory} from 'react-router-dom';

/**
 *
 * @param {*} props
 * @return {object}
 */
function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        GroupTwentyOne
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}
const theme = createTheme();

/**
 * Credit to https://github.com/mui-org/material-ui/tree
 * /master/docs/src/pages/getting-started/templates/sign-in
 * @return {object}
 */
export default function LoginPage() {
  const history = useHistory();
  const updateUser = useUserUpdate();
  const [unsucessfulLogin, setUnsucessfulLogin] = React.useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const userInfo = {email: data.get('email'), password: data.get('password')};
    try {
      const response = await fetch('/login', {
        method: 'POST',
        body: JSON.stringify(userInfo),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const info = await response.json();
      localStorage.setItem('user', JSON.stringify(info));
      updateUser(info);
      setUnsucessfulLogin(false);
      history.push('/');
    } catch (execption) {
      setUnsucessfulLogin(true);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{m: 1, bgcolor: 'secondary.main'}}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          { unsucessfulLogin &&
              <Typography
                id="invalid-login"
                sx={{color: 'red', mt: 1}}
                component="h1"
                variant="body2"
              >
                Incorrect Email or Password
              </Typography>
          }
          <Box
            component="form"
            onSubmit={handleSubmit}
            noValidate sx={{mt: 1}}
          >
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              aria-label="Email Address"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              aria-label="Password"
              autoComplete="current-password"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              aria-label="Login Button"
              variant="contained"
              sx={{mt: 3, mb: 2}}
            >
              Sign In
            </Button>
            <Grid container justifyContent="center">
              <Grid item>
                <Link id="sign-up" href="/register" variant="body2">
                  {'Don\'t have an account? Sign Up'}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{mt: 8, mb: 4}} />
      </Container>
    </ThemeProvider>
  );
}
