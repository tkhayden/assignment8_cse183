import React, {useContext, useState} from 'react';

export const CategoryContext = React.createContext();

export const CategoryUpdateContext = React.createContext();

export const useCategory = () => {
  return useContext(CategoryContext);
};

export const useCategoryUpdate = () =>{
  return useContext(CategoryUpdateContext);
};

const CategoryProvider = ({children}) => {
  const [category, setCategory] = useState([]);

  return (
    <CategoryContext.Provider value={category}>
      <CategoryUpdateContext.Provider value= {setCategory}>
        {children}
      </CategoryUpdateContext.Provider>
    </CategoryContext.Provider>
  );
};

export default CategoryProvider;
