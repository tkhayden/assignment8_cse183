import React from 'react';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';

// creadit to MUI team @ https://mui.com/components/paper/
// creadit to MUI team @ https://mui.com/api/input-base/
// creadit to MUI team @ https://mui.com/components/buttons/
// creadit to MUI team @ https://mui.com/components/icons/

const SearchBar = ({setListings}) => {
  const handleSearchInput = async (event) => {
    if (/\S/.test(event.target.value)) {
      fetch(`/v0/listing?search=${event.target.value}`)
        .then((response) => {
          if (!response.ok) {
            throw response;
          }
          return response.json();
        })
        .then((json) => {
          setListings(json);
        })
        .catch((error) => {
          setListings([]);
        });
    }
  };

  return (
    <Paper component="form" sx={{m: 1}}>
      <IconButton
        disabled
        sx={{p: '10px', top: '-50%', margin: 0}}
        color="secondary"
      >
        <SearchIcon />
      </IconButton>
      <InputBase
        sx={{ml: 1, flex: 1, width: '70%', top: '-40%', margin: 0}}
        id="searchBar"
        inputProps={{'data-testid': 'search'}}
        aria-label="search listings"
        placeholder="Search"
        onChange={(event) => handleSearchInput(event)}
        onKeyDown={(event) => {
          if (event.code === 'Enter') event.preventDefault();
        }}
      />
    </Paper>
  );
};

export default SearchBar;
