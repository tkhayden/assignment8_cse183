import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Fab from '@mui/material/Fab';
import CloseIcon from '@mui/icons-material/Close';
import Divider from '@mui/material/Divider';
import Button from '@mui/material/Button';
import {useUser} from './providers/UserProvider';
import {useHistory} from 'react-router-dom';


const desktopClasses = makeStyles((theme) => ({
  root: {
    'flexGrow': 1,
    'display': 'flex',
    'flexWrap': 'wrap',
    '& > *': {
      width: '800px',
      height: '100%',
      zIndex: 10000,
      bottom: 0,
      position: 'fixed',
      left: '50%',
      transform: 'translateX(-50%)',
    },
  },
  body: {
    position: 'relative',
  },
  image: {
    height: '55vh',
    width: '100%',
  },
  pages: {
    marginTop: theme.spacing(1),
    justifyContent: 'center',
    display: 'flex',
  },
  closebtn: {
    position: 'absolute',
    top: '1%',
    left: '90%',
  },
  loginBtn: {
    justifyContent: 'center',
    display: 'flex',
  },
  content: {
    width: '100%',
  },
}));

const mobileClasses = makeStyles((theme) => ({
  root: {
    'flexGrow': 1,
    'display': 'flex',
    'flexWrap': 'wrap',
    'height': '100vh',
    '& > *': {
      height: '100%',
      width: '100%',
      zIndex: 10000,
      position: 'fixed',
      left: 0,
      top: 0,
      bottom: 0,
    },
  },
  body: {
    position: 'relative',
  },
  image: {
    height: '45vh',
    width: '100%',
  },
  pages: {
    marginTop: theme.spacing(1),
    justifyContent: 'center',
    display: 'flex',
  },
  closebtn: {
    position: 'absolute',
    top: '1vh',
    left: '87vw',
  },
  loginBtn: {
    justifyContent: 'center',
    display: 'flex',
  },
  content: {
    width: '100%',
  },
}));

// creadit to MUI team @ https://mui.com/components/Paper/
// creadit to MUI team @ https://mui.com/components/typography/
// creadit to MUI team @ https://mui.com/components/pagination/
// creadit to MUI team @ https://mui.com/api/fab/
// creadit to MUI team @ https://mui.com/components/icons/
// creadit to MUI team @ https://mui.com/components/buttons/
// creadit to MUI team @ https://mui.com/components/dividers/


/**
 * @return {*}
 */
const ListingViewer = ({width, displayedListing, handleViewerClose, open}) => {
  const classes = width < 800 ? mobileClasses() : desktopClasses();
  const [currentPage, setCurrentPage] = React.useState(1);
  const user = useUser();
  const history = useHistory();

  const handlePageChange = (event, value) => {
    setCurrentPage(value);
  };

  React.useEffect(() => {
    if (open) {
      document.body.style.overflow = 'unset';
    } else {
      document.body.style.overflow = 'hidden';
    }
  }, [open]);

  return (
    <div
      className={classes.root}
    >
      <Paper style={{overflow: 'auto'}}>
        <div className={classes.body} >
          <img
            className={classes.image}
            src={displayedListing?.listing?.images[currentPage-1]}
            alt={displayedListing?.listing?.images[0]}
            aria-label="listingViewerImage"
          />
          <Fab
            className={classes.closebtn}
            aria-label="Close Viewer"
            color="primary"
            size="small"
            onClick={() => {
              setCurrentPage(1); handleViewerClose();
            }}
          >
            <CloseIcon />
          </Fab>

          <Divider variant="middle" sx={{m: 1}}/>
          <Pagination
            className={classes.pages}
            count={displayedListing?.listing?.images?.length}
            page={currentPage}
            onChange={handlePageChange}
          />
          <Divider variant="middle" sx={{m: 1}}/>

          { !user &&
              <div className={classes.loginBtn}>
                <Button
                  variant="contained"
                  aria-label="Unauthed Viewer Login"
                  fullWidth
                  onClick={() => history.push('/login')}
                >
                    Login for more details
                </Button>
              </div>
          }

          <Typography sx={{m: 1, fontWeight: 'bold'}} variant="h6" >
            {displayedListing?.listing?.title}
          </Typography>
          <div className={classes.content} >
            <Typography sx={{m: 1}} variant="body1" gutterBottom >
              <b>Price: </b>${displayedListing?.listing?.price}
            </Typography>
            <Typography sx={{m: 1}} variant="body1" gutterBottom >
              <b>Description: </b>{displayedListing?.listing?.description}
            </Typography>
          </div>
          { user &&
            <div>
              <Divider variant="middle" sx={{m: 1}}/>
              <Typography sx={{m: 1, fontWeight: 'bold'}} variant="h6" >
                Replies:
              </Typography>
              <div aria-label="replies">
                {displayedListing?.listing?.replies?.map((reply) => (
                  <div key={reply?.datePosted}>
                    <Typography sx={{m: 1}} variant="body2" >
                      <b>{reply?.userName}: </b> {reply?.content}
                    </Typography>
                    <Divider variant="inset" sx={{m: 1, width: '50%'}}/>
                  </div>
                ))}
              </div>
            </div>
          }
        </div>
      </Paper>
    </div>
  );
};

export default ListingViewer;


