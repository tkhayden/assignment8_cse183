import React from 'react';
import ImageList from '@mui/material/ImageList';
import {makeStyles} from '@material-ui/core/styles';
import ImageListItem from '@mui/material/ImageListItem';
import Typography from '@mui/material/Typography';

const useStyles = makeStyles((theme) => ({
  image: {
    background: '#E9EBEE',
    borderRadius: '10px 10px 10px 10px',
  },
  body: {
    paddingLeft: '5px',
    fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
  },
  price: {
    paddingLeft: '5px',
    fontSize: '18px',
    fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
  },
}));

const Listings = ({width, listings, handleListingClick}) => {
  const classes = useStyles();
  const getColumns = (width) => {
    if (width <= 750) {
      return 2;
    } else if ( width <= 1300) {
      return 3;
    } else {
      return 4;
    }
  };

  // creadit to MUI team @ https://mui.com/components/image-list/
  // creadit to MUI team @ https://mui.com/components/typography/

  return (
    <>
      <ImageList cols={getColumns(width)} aria-label="listings">
        {listings.map((item) => (
          <ImageListItem
            itemID={item?.id}
            id={item?.id}
            aria-label="listing"
            key={item?.id}
            className={classes.image}
            onClick={(e) => handleListingClick(e.target.getAttribute('itemID'))}
          >
            <img
              itemID={item?.id}
              className={classes.image}
              src={`${item?.listing?.images[0]}`}
              srcSet={`${item?.listing?.images[0]}`}
              alt={item?.listing?.title}
              loading="lazy"
            />
            <div className={classes.body} itemID={item?.id}>
              <Typography
                itemID={item?.id}
                variant="h6"
                className={classes.price}
              >
                {`$${item?.listing?.price}`}
              </Typography>
              <Typography
                itemID={item?.id}
                variant="body1"
              >
                {item?.listing?.title}
              </Typography>
              <Typography
                itemID={item?.id}
                variant="body2"
                color="textSecondary"
                gutterBottom
              >
                {`${item?.listing?.location?.city},
                ${item?.listing?.location?.state}`}
              </Typography>
            </div>
          </ImageListItem>
        ))}
      </ImageList>
    </>
  );
};

export default Listings;
