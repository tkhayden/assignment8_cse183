import React, {useState, useEffect} from 'react';
import Box from '@mui/material/Box';
import Listings from './Listings';
import ListingViewer from './ListingViewer';
import HeaderBar from './HeaderBar';
import SearchBar from './SearchBar';
import Categories from './Categories';
import SubCategoryBar from './SubCategoryBar';
import {useUserUpdate} from './providers/UserProvider';

// creadit to MUI team @ https://mui.com/components/box/

/**
 *
 * @return {object}
 */
export default function Home() {
  const [dimensions, setDimensions] = useState(window.innerWidth);
  const [listings, setListings] = React.useState([]);
  const [viewerClose, setViewerClose] = React.useState(true);
  const [displayedListing, setDisplayedListing] = React.useState({});


  const [open, setOpen] = useState(false);
  const updateUser = useUserUpdate();


  useEffect(() => {
    const loggedUser = JSON.parse(localStorage.getItem('user'));
    loggedUser ? updateUser(loggedUser) : updateUser(null);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // credit to David Harrison from Lecture 9 slides
  useEffect(() =>{
    const handleResize = () => {
      setDimensions(window.innerWidth);
    };
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, [dimensions]);

  useEffect(() => {
    fetch('/v0/listing')
      .then((response) => {
        if (!response.ok) {
          throw response;
        }
        return response.json();
      })
      .then((json) => {
        setListings(json);
      })
      .catch((error) => {
        setListings([]);
      });
    return () => {
      setListings([]);
    };
  }, []);

  const handleViewerClose = () => {
    setViewerClose(true);
  };

  const handleListingClick = async (id) => {
    fetch(`/v0/listing/${id}`)
      .then((response) => {
        if (!response.ok) {
          throw response;
        }
        return response.json();
      })
      .then((json) => {
        setDisplayedListing(json);
      })
      .catch((error) => {
        setListings([]);
      });
    setViewerClose(false);
  };

  return (
    <div style={{display: 'flex'}}>
      <Box component="nav" style={{
        pointerEvents: viewerClose ? '' : 'none',
        opacity: viewerClose ? '' : '0.1',
      }}
      >
        <HeaderBar width={dimensions} />
      </Box>

      <Categories dimensions={dimensions} open={open} setOpen={setOpen}
        setListings={setListings}/>
      <Box
        component="main"
        sx={{flexGrow: 1, marginTop: 8}}
      >
        <div hidden={viewerClose}>
          <ListingViewer
            width={dimensions}
            displayedListing={displayedListing}
            open={viewerClose}
            handleViewerClose={handleViewerClose}
          />
        </div>
        <SubCategoryBar dimensions={dimensions}
          setOpen={setOpen} setListing={setListings}/>


        { dimensions < 800 && <SearchBar setListings={setListings}/>}
        <div style={{
          pointerEvents: viewerClose ? '' : 'none',
          opacity: viewerClose ? '' : '0.1',
        }}
        >
          <Listings
            width={dimensions}
            listings={listings}
            setListings={setListings}
            handleListingClick={handleListingClick}
          />
        </div>
      </Box>
    </div>
  );
}
