import React, {useState, useEffect} from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import {Divider, Typography} from '@mui/material';
import {useCategoryUpdate} from './providers/CategoryProvider';

// creadit to MUI team @ https://mui.com/components/typography/
// creadit to MUI team @ https://mui.com/components/box/
// creadit to MUI team @ https://mui.com/components/drawers/
// creadit to MUI team @ https://mui.com/components/lists/
// creadit to MUI team @ https://mui.com/components/dividers/
// creadit to MUI team @ https://mui.com/api/toolbar/

const Categories = ({dimensions, open, setOpen, setListings}) => {
  const [categories, setCategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const updateCategory = useCategoryUpdate();
  let width;
  let drawer;
  let openProp;
  let paddingL;
  useEffect(() => {
    fetch('/v0/category')
      .then((response) => {
        if (!response.ok) {
          throw response;
        }
        return response.json();
      })
      .then((json) => {
        setCategories(json);
      })
      .catch((error) => {
        setCategories([]);
      });
    return () => {
      setCategories([]);
    };
  }, []);

  useEffect(() => {
    if (dimensions >= 800) {
      setOpen(false);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  if ((dimensions) < 800) {
    width = '100%';
    drawer = 'temporary';
    openProp = open;
    paddingL = '27%';
  } else {
    width = 300;
    drawer = 'permanent';
    openProp = true;
    paddingL = '3%';
  }


  const handleCategoryClick = async (e) => {
    const id = e.currentTarget.id;
    setSelectedCategory(e.currentTarget.id);
    const response = await fetch(`/v0/category/${id}`);
    const info = await response.json();
    const listing = await fetch(`/v0/listing?category=${id}`);
    const lis = await listing.json();
    setListings(lis);
    updateCategory([info[0]]);
  };

  return (
    <>

      <Drawer
        variant= {drawer}
        open = {openProp}
        sx={{
          width: width,
          flexShrink: 0,
          [`& .MuiDrawer-paper`]: {width: width, boxSizing: 'border-box'},
        }}
      >
        <Toolbar />
        <Box sx={{overflow: 'auto'}}>
          <Typography variant="h6" component="div"
            sx={{paddingLeft: paddingL, paddingTop: '3%', paddingBottom: '3%'}}>
           Select Category
          </Typography>
          <Divider/>
          <List>
            {categories.map((elem) => (
              <ListItem button key={elem.id}
                id={elem.id}
                aria-label={elem.name}
                selected={selectedCategory === elem.id}
                onClick={(e) => {
                  setOpen(false); handleCategoryClick(e );
                }}
              >
                <ListItemIcon>
                  <img src={elem.category.icon} alt={elem.category.name}/>
                </ListItemIcon>
                <ListItemText primary={elem.category.name} />
              </ListItem>
            ))}
          </List>
        </Box>
      </Drawer>
    </>
  );
};

export default Categories;
