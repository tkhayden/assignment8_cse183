import React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import {useCategory, useCategoryUpdate} from './providers/CategoryProvider';
import Chip from '@mui/material/Chip';
import {Typography} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

// creadit to MUI team @ https://mui.com/components/typography/
// creadit to MUI team @ https://mui.com/components/box/
// creadit to MUI team @ https://mui.com/components/paper/
// creadit to MUI team @ https://mui.com/components/chip/
// creadit to MUI team @ https://mui.com/components/icons/

const SubCategoryBar = ({dimensions, setOpen, setListing}) => {
  const category = useCategory();
  const updateCategory = useCategoryUpdate();

  const subCategoryClick = async (e) => {
    const id = e.currentTarget.id;
    const response = await fetch(`/v0/category/${id}`);
    const info = await response.json();

    const listing = await fetch(`/v0/listing?category=${id}`);
    const lisingInfo = await listing.json();
    setListing(lisingInfo);
    updateCategory([...category, info[0]]);
  };
  const marginVal = (dimensions) => {
    let value = '17px';
    if (dimensions < 800) {
      value = '8px';
    }
    return value;
  };
  const setSubCategories = (category) => {
    const subcategory = category[category.length-1].subcategories;
    return (
      <>
        {subcategory.map((elem) =>(
          <Chip key={elem.category.name}
            sx={{margin: '5px', marginLeft: marginVal(dimensions)}}
            id={elem.id}
            label={elem.category.name}
            onClick = {(e) => subCategoryClick(e)} >
          </Chip>
        ),
        )}
      </>
    );
  };
  const desktopView = (category) => {
    if (category.length > 0) {
      const subcategory = category[category.length-1].subcategories;
      if (subcategory.length > 0) {
        return (
          <Box
            sx={{
              'display': 'flex',
              'flexWrap': 'wrap',
              '& > :not(style)': {
                m: 1,
                width: '100%',
                height: 80,
                borderRadius: '25px',

              },
            }}
            aria-label='desktop sub-category bar'
          >

            <Paper elevation={3} sx={{
              'alignItems': 'center'}}>
              <Typography variant="h7" component="div"
                sx={{fontWeight: 'bold', padding: '7px', paddingLeft: '17px'}}>
            Shop By Category
              </Typography>
              {setSubCategories(category)}
            </Paper>

          </Box>
        );
      }
      return null;
    } else {
      return null;
    }
  };
  // Centering the icon and Text. Credit to Peter Moses answer: https://stackoverflow.com/questions/51940157/how-to-align-horizontal-icon-and-text-in-mui
  const mobileView = (category) => {
    if (category.length > 0) {
      return (
        <Box>
          <div style={{
            display: 'flex',
            alignItems: 'center',
            flexWrap: 'wrap',
          }}
          onClick={() =>
            setOpen(true)
          }>
            <Typography variant="h5" component="div"
              sx={{fontWeight: 'bold', paddingBottom: '5px',
                marginTop: '-5px', marginLeft: '7px'}}
              aria-label={category[category.length-1].category}
            >
              {category[category.length-1].category}
            </Typography>
            <ExpandMoreIcon sx={{paddingBottom: '4px'}}/>
          </div>
          {setSubCategories(category)}
        </Box>
      );
    } else {
      return (
        <>
          <Chip label="All Categories"
            aria-label='All Categories'
            onClick={() => setOpen(true)}/>
        </>
      );
    }
  };
  return (
    <>
      {dimensions < 800 ?
        mobileView(category) :
        desktopView(category)}
    </>
  );
};

export default SubCategoryBar;
