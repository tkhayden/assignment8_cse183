import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

// import Dummy from './Dummy';
import LoginPage from './LoginPage';
import RegPage from './RegPage';
import Home from './Home';
import UserProvider from './providers/UserProvider';
import CategoryProvider from './providers/CategoryProvider';

/**
 * Simple component with no state.
 *
 * @return {object} JSX
 */
function App() {
  return (
    <BrowserRouter>
      <UserProvider>
        <CategoryProvider>
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/login">
              <LoginPage />
            </Route>
            <Route path="/register">
              <RegPage />
            </Route>
          </Switch>
        </CategoryProvider>
      </UserProvider>
    </BrowserRouter>
  );
}

export default App;
