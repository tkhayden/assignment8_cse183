import React, {useState} from 'react';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import {useHistory} from 'react-router-dom';
import TextField from '@mui/material/TextField';
import {useUser, useUserUpdate} from './providers/UserProvider';

// credit to MUI team @: https://mui.com/components/app-bar/
// creadit to MUI team @ https://mui.com/components/buttons/
// creadit to MUI team @ https://mui.com/components/typography/
// creadit to MUI team @ https://mui.com/components/text-fields/
// creadit to MUI team @ https://mui.com/api/toolbar/

const HeaderBar = ({width}) => {
  const history = useHistory();
  const [userInfo, setUserInfo] = useState({email: '', password: ''});
  const user = useUser();
  const updateUser = useUserUpdate();

  const handleLogOut = (e) => {
    e.preventDefault();
    localStorage.removeItem('user');
    updateUser(null);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch('/login', {
        method: 'POST',
        body: JSON.stringify(userInfo),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const info = await response.json();
      localStorage.setItem('user', JSON.stringify(info));
      updateUser(info);
    } catch (execption) {
      history.push('/login');
    }
  };

  const setToolBar = (screenWidth) => {
    if (screenWidth <= 800) {
      return (
        <>
          <Button color="inherit" aria-label="log-in button"
            onClick={() => history.push('/login')}>Login</Button>
        </>
      );
    } else {
      return (
        <form onSubmit = {onSubmit}>
          <TextField id="filled"
            label="Email"
            autoFocus
            aria-label="email quick log-in"
            variant="filled"
            size='small'
            onChange={({target}) =>
              setUserInfo({...userInfo, email: target.value})}
            sx={{backgroundColor: 'white', width: 170, marginRight: 1}} />
          <TextField id="filled-basic"
            autoFocus
            label="Password"
            type="password"
            aria-label="password quick log-in"
            variant="filled"
            size='small'
            onChange={({target}) =>
              setUserInfo({...userInfo, password: target.value})}
            sx={{backgroundColor: 'white', width: 170, marginRight: 1}} />
          <Button color="inherit"
            type ='submit'
            value="Submit"
            aria-label="log-in submit button">Login</Button>
        </form>
      );
    }
  };
  return (
    <AppBar position="fixed" sx={{width: '100%',
      zIndex: 10000}}>
      <Toolbar>
        <Typography variant="h6" component="div" sx={{flexGrow: 1}}>
            Facebook
        </Typography>
        {user ?
          <>
            <Typography variant="h7" component="div" aria-label="users email">
              {user.email}
            </Typography>
            <Button color="inherit"
              aria-label="log-out button"
              onClick ={(e) => handleLogOut(e)}
            >Log out</Button>
          </> :
          setToolBar(width)}
      </Toolbar>

    </AppBar>
  );
};

export default HeaderBar;
