import {render, screen, waitFor, fireEvent} from '@testing-library/react';
import {allListingsURL, getCategoriesURL,
  listingData, categoriesData, invalidListingIdURL} from './MockData';
// import userEvent from '@testing-library/user-event';
import {act} from 'react-dom/test-utils';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import '@testing-library/jest-dom';
import Home from '../App';

const server = setupServer(
  rest.get(allListingsURL, (req, res, ctx) => {
    return res(ctx.json(listingData));
  }),
  rest.get(getCategoriesURL, (req, res, ctx) => {
    return res(ctx.json(categoriesData));
  }),
  rest.get(invalidListingIdURL, (req, res, ctx) => {
    return res(ctx.status(500));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('Home Renders', async () => {
  render(<Home />);
  screen.getByText('Facebook');
});

test('Log-in button is clickable', async () => {
  render(<Home />);
  screen.getByRole('button', {
    name: 'log-in submit button',
  });
});

test('Handles Server Error For /v0/listings endpoint', async () => {
  server.use(
    rest.get(allListingsURL, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );
  render(<Home />);
  await waitFor(() => screen.getByLabelText('listings'));
});

test('Handles Server Error For /v0/listings/:id endpoint', async () => {
  server.use(
    rest.get(invalidListingIdURL, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );
  render(<Home />);
  await waitFor(() => screen.getByLabelText('listings'));
  await waitFor(() => screen.getByText('FAKE ITEM'));
  const listing = screen.getByText('FAKE ITEM');
  fireEvent.click(listing);
  await waitFor(() => screen.getByLabelText('listings'));
  try {
    // NO listings should appear on error
    await waitFor(() => screen.getByLabelText('listing'));
  } catch (e) {
    // eslint-disable-next-line jest/no-conditional-expect
    expect(e.message).not.toBeNull();
  }
});

test('Window Resize', async () => {
  render(<Home />);
  act(() => {
    global.innerWidth = 500;
    global.dispatchEvent(new Event('resize'));
  });
  screen.getByText('Facebook');
  await waitFor(() => screen.getByLabelText('listings'));
});

