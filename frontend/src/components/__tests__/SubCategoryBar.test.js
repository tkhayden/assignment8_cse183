import {render, screen, fireEvent, waitFor} from '@testing-library/react';
import {
  listingData, categoriesData,
  categoryIDdata, subCategoryData} from './MockData';
import '@testing-library/jest-dom';
import {act} from 'react-dom/test-utils';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import SubCategoryBar from '../SubCategoryBar';
import {CategoryContext} from '../providers/CategoryProvider';
import {CategoryUpdateContext} from '../providers/CategoryProvider';

const URL = '/v0/category';

const server = setupServer(
  rest.get(URL, (req, res, ctx) => {
    return res(ctx.json(categoriesData));
  }),
  rest.get(`${URL}/3fc8800d-56bd-41bb-b103-026c0e1453da`,
    (req, res, ctx) => {
      return res(ctx.json(subCategoryData[1]));
    }),
  rest.get('/v0/listing', (req, res, ctx) => {
    return res(ctx.json(listingData));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());
describe('Desktop View Testing', () => {
  beforeAll(()=> {
    act(() => {
      global.innerWidth = 1100;
      global.dispatchEvent(new Event('resize'));
    });
  });
  test('No Category: selected', async () => {
    const mock = jest.fn();
    render(
      <CategoryContext.Provider value={[]}>
        <SubCategoryBar dimensions={1100} setOpen={mock} setListing={mock} />

      </CategoryContext.Provider>,

    );
    await waitFor(() =>
      expect(screen.queryByLabelText('desktop sub-category bar')).toBeNull());
  });
  test('Category: Vehicle selected', async () => {
    const mock = jest.fn();
    render(
      <CategoryContext.Provider value={[categoryIDdata[0]]}>
        <SubCategoryBar dimensions={1100} setOpen={mock} setListing={mock} />

      </CategoryContext.Provider>,

    );
    await waitFor(() => screen.getAllByLabelText('desktop sub-category bar'));
    await waitFor(() => screen.getByText('Trucks'));
    await waitFor(() => screen.getByText('Cars'));
  });
  test('Category: Vehicle. Sub-category Trucks selected', async () => {
    server.use(
      rest.get('/v0/category/ed5a6dc4-eabf-4412-83ba-bdacb37de451',
        (req, res, ctx) => {
          return res(ctx.json([categoryIDdata[1]]));
        }),
    );
    const mock = jest.fn();
    const contextMock = jest.fn();
    render(
      <CategoryContext.Provider value={[categoryIDdata[0]]}>
        <CategoryUpdateContext.Provider value={contextMock}>
          <SubCategoryBar dimensions={1100} setOpen={mock} setListing={mock} />
        </CategoryUpdateContext.Provider>
      </CategoryContext.Provider>,

    );
    fireEvent.click(screen.getByText('Cars'));
    await waitFor(() => expect(contextMock).toHaveBeenCalledTimes(1));
    // await waitFor(()=> screen.debug());
  });
  test('Category: Cars with no subcategories', async () => {
    const mock = jest.fn();
    render(
      <CategoryContext.Provider value={[categoryIDdata[1]]}>

        <SubCategoryBar dimensions={1100} setOpen={mock} setListing={mock} />

      </CategoryContext.Provider>,

    );
    await waitFor(() =>
      expect(screen.queryByLabelText('desktop sub-category bar')).toBeNull());
  });
});
describe('Mobile View Testing', () => {
  beforeAll(()=> {
    act(() => {
      global.innerWidth = 500;
      global.dispatchEvent(new Event('resize'));
    });
  });
  test('No Category: selected', async () => {
    const mock = jest.fn();
    render(
      <CategoryContext.Provider value={[]}>
        <SubCategoryBar dimensions={500} setOpen={mock} setListing={mock} />

      </CategoryContext.Provider>,

    );
    await waitFor(() =>
      expect(screen.getByText('All Categories')));
  });
  test('Category: Vehicle selected', async () => {
    const mock = jest.fn();
    render(
      <CategoryContext.Provider value={[categoryIDdata[0]]}>
        <SubCategoryBar dimensions={500} setOpen={mock} setListing={mock} />

      </CategoryContext.Provider>,

    );
    await waitFor(() => screen.getByText('Vehicles'));
    await waitFor(() => screen.getByText('Trucks'));
    await waitFor(() => screen.getByText('Cars'));
  });
  test('Category: Vehicle. Sub-category Trucks selected', async () => {
    server.use(
      rest.get('/v0/category/ed5a6dc4-eabf-4412-83ba-bdacb37de451',
        (req, res, ctx) => {
          return res(ctx.json([categoryIDdata[1]]));
        }),
    );
    const mock = jest.fn();
    const contextMock = jest.fn();
    render(
      <CategoryContext.Provider value={[categoryIDdata[0]]}>
        <CategoryUpdateContext.Provider value={contextMock}>
          <SubCategoryBar dimensions={500} setOpen={mock} setListing={mock} />
        </CategoryUpdateContext.Provider>
      </CategoryContext.Provider>,

    );
    fireEvent.click(screen.getByText('Cars'));
    await waitFor(() => expect(contextMock).toHaveBeenCalledTimes(1));
    // await waitFor(()=> screen.debug());
  });
  test('Category: Cars with no subcategories', async () => {
    const mock = jest.fn();
    render(
      <CategoryContext.Provider value={[categoryIDdata[1]]}>

        <SubCategoryBar dimensions={500} setOpen={mock} setListing={mock} />

      </CategoryContext.Provider>,

    );
    await waitFor(() =>
      expect(screen.queryByLabelText('All Categories')).toBeNull());
  });
  test('click on All Categories button', async () => {
    const mock = jest.fn();
    const open = jest.fn();
    render(
      <CategoryContext.Provider value={[]}>

        <SubCategoryBar dimensions={500} setOpen={open} setListing={mock} />

      </CategoryContext.Provider>,

    );
    await waitFor(() =>
      fireEvent.click(screen.getByText('All Categories')));
    await waitFor(() => expect(open).toHaveBeenCalledTimes(1));
  });
  test('click on Category title button', async () => {
    const mock = jest.fn();
    const open = jest.fn();
    render(
      <CategoryContext.Provider value={[categoryIDdata[0]]}>

        <SubCategoryBar dimensions={500} setOpen={open} setListing={mock} />

      </CategoryContext.Provider>,

    );
    await waitFor(() =>
      fireEvent.click(screen.getByLabelText('Vehicles')));
    await waitFor(() => expect(open).toHaveBeenCalledTimes(1));
  });
});
