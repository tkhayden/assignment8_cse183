import {render, screen, fireEvent, waitFor} from '@testing-library/react';
import {allListingsURL, getCategoriesURL, listingByIdURL,
  listingData, categoriesData} from './MockData';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import {act} from 'react-dom/test-utils';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import ListingViewer from '../App';

const URL = '/login';

const server = setupServer(
  rest.post(URL, (req, res, ctx) => {
    return res(ctx.json({id: 'testid',
      email: 'test@gmail.com', accessToken: '432323232fds'}));
  }),
  rest.get(allListingsURL, (req, res, ctx) => {
    return res(ctx.json(listingData));
  }),
  rest.get(listingByIdURL, (req, res, ctx) => {
    return res(ctx.json(listingData[0]));
  }),
  rest.get(getCategoriesURL, (req, res, ctx) => {
    return res(ctx.json(categoriesData));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());
beforeEach(() => {
  window.history.pushState({}, 'Home', '/' );
});

describe('Desktop View', () => {
  beforeAll(()=> {
    act(() => {
      global.innerWidth = 1100;
      global.dispatchEvent(new Event('resize'));
    });
  });

  test('Click on Listing in Desktop View', async () => {
    render(<ListingViewer />);
    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listing = screen.getByText('2012 Ford Ranger');
    fireEvent.click(listing);
    await waitFor(() => screen.getByLabelText('listingViewerImage'));
  });

  test('Change Image in ListingViewer in Desktop View', async () => {
    render(<ListingViewer />);
    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listing = screen.getByText('2012 Ford Ranger');
    await waitFor(() => userEvent.click(listing));

    await waitFor(() => screen.getByRole('button', {name: /Go to next page/i}));
    const nextImage = screen.getByRole('button', {name: /Go to next page/i});
    await waitFor(() => userEvent.click(nextImage));

    // Page two is now set as current page since next button was hit once
    await waitFor(() => screen.getByRole('button', {name: /page 2/i}));
    screen.getByRole('button', {name: /page 2/i});
  });

  test('Open and Close ListViewer in Desktop View', async () => {
    render(<ListingViewer />);
    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listing = screen.getByText('2012 Ford Ranger');
    await waitFor(() => userEvent.click(listing));

    await waitFor(() => screen.getByRole('button', {name: /Close Viewer/i}));
    const closeButton = screen.getByRole('button', {name: /Close Viewer/i});
    await waitFor(() => userEvent.click(closeButton));
  });

  test('Open ListViewer and Click on Unauthed User Login Button', async () => {
    render(<ListingViewer />);
    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listing = screen.getByText('2012 Ford Ranger');
    await waitFor(() => userEvent.click(listing));

    await waitFor(() => screen.getByRole('button',
      {name: /Unauthed Viewer Login/i}));
    const unauthedLoginButton = screen.getByRole('button',
      {name: /Unauthed Viewer Login/i});
    await waitFor(() => userEvent.click(unauthedLoginButton));
  });

  test('User Quick Login Desktop View to View Replies', async () => {
    render(<ListingViewer/>);
    fireEvent.change(screen.getByLabelText('email quick log-in')
      .querySelector('input'), {
      target: {
        value: 'password',
      },
    });
    fireEvent.change(screen.getByLabelText('password quick log-in')
      .querySelector('input'), {
      target: {
        value: 'test@gmail.com',
      },
    });
    act(() => {
      fireEvent.click(screen.getByLabelText('log-in submit button'));
    });
    await waitFor(() => screen.getByText('test@gmail.com'));
    await waitFor(() => screen.getByLabelText('log-out button'));

    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listing = screen.getByText('2012 Ford Ranger');
    await waitFor(() => userEvent.click(listing));

    await waitFor(() => screen.getByText('Replies:'));
    await waitFor(() => screen.getByLabelText('replies'));
    await waitFor(() => screen.getByText('Barry Gallanders:'));

    act(() => {
      fireEvent.click(screen.getByLabelText('log-out button'));
    });
    await waitFor(() => screen.getByLabelText('email quick log-in'));
    await waitFor(() => screen.getByLabelText('password quick log-in'));
  });
});

describe('Mobile View', () => {
  beforeAll(()=> {
    act(() => {
      global.innerWidth = 500;
      global.dispatchEvent(new Event('resize'));
    });
  });

  test('Click on Listing in Mobile View', async () => {
    render(<ListingViewer />);
    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listing = screen.getByText('2012 Ford Ranger');
    fireEvent.click(listing);
    await waitFor(() => screen.getByLabelText('listingViewerImage'));
  });

  test('Change Image in ListingViewer in Mobile View', async () => {
    render(<ListingViewer />);
    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listing = screen.getByText('2012 Ford Ranger');
    await waitFor(() => userEvent.click(listing));

    await waitFor(() => screen.getByRole('button', {name: /Go to next page/i}));
    const nextImage = screen.getByRole('button', {name: /Go to next page/i});
    await waitFor(() => userEvent.click(nextImage));

    // Page two is now set as current page since next button was hit once
    await waitFor(() => screen.getByRole('button', {name: /page 2/i}));
    screen.getByRole('button', {name: /page 2/i});
  });

  test('Open and Close ListViewer in Mobile View', async () => {
    render(<ListingViewer />);
    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listing = screen.getByText('2012 Ford Ranger');
    await waitFor(() => userEvent.click(listing));

    await waitFor(() => screen.getByRole('button', {name: /Close Viewer/i}));
    const closeButton = screen.getByRole('button', {name: /Close Viewer/i});
    act(() =>userEvent.click(closeButton));
  });

  test('Open ListViewer and Click on Unauthed User Login Button', async () => {
    render(<ListingViewer />);
    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listing = screen.getByText('2012 Ford Ranger');
    await waitFor(() => userEvent.click(listing));

    await waitFor(() => screen.getByRole('button',
      {name: /Unauthed Viewer Login/i}));
    const unauthedLoginButton = screen.getByRole('button',
      {name: /Unauthed Viewer Login/i});
    act(() => userEvent.click(unauthedLoginButton));
  });
});
