import {render, screen, fireEvent, waitFor} from '@testing-library/react';
import {allListingsURL, listingData} from './MockData';
import '@testing-library/jest-dom';
import {act} from 'react-dom/test-utils';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import Listings from '../Listings';


const server = setupServer(
  rest.get(allListingsURL, (req, res, ctx) => {
    return res(ctx.json(listingData));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());


describe('Desktop View', () => {
  beforeAll(()=> {
    act(() => {
      global.innerWidth = 1100;
      global.dispatchEvent(new Event('resize'));
    });
  });
  test('Listings Visible in Desktop View', async () => {
    const listingClick = (json) => {
      return json;
    };
    render(
      <Listings
        width={1100}
        listings={listingData}
        handleListingClick={listingClick}
      />,
    );
    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listings = screen.getByLabelText('listings');
    expect(listings.children.length).toBe(6);
  });
});

describe('Wide View', () => {
  beforeAll(()=> {
    act(() => {
      global.innerWidth = 1400;
      global.dispatchEvent(new Event('resize'));
    });
  });

  test('Listings Visible in Wide View', async () => {
    const listingClick = (json) => {
      return json;
    };
    render(
      <Listings
        width={1400}
        listings={listingData}
        handleListingClick={listingClick}
      />,
    );
    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listings = screen.getByLabelText('listings');
    expect(listings.children.length).toBe(6);
  });
});

describe('Mobile View', () => {
  beforeAll(()=> {
    act(() => {
      global.innerWidth = 500;
      global.dispatchEvent(new Event('resize'));
    });
  });

  test('Listings Visible', async () => {
    const listingClick = (json) => {
      return json;
    };
    render(
      <Listings
        width={500}
        listings={listingData}
        handleListingClick={listingClick}
      />,
    );
    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listings = screen.getByLabelText('listings');
    expect(listings.children.length).toBe(6);
  });

  test('Click on Listing', async () => {
    const listingClick = (id) => {
      return listingData[0];
    };
    render(
      <Listings
        width={500}
        listings={listingData}
        handleListingClick={listingClick}
      />,
    );
    await waitFor(() => screen.getByText('2012 Ford Ranger'));
    const listing = screen.getByText('2012 Ford Ranger');
    fireEvent.click(listing);
    await new Promise((r) => setTimeout(r, 2500));
  });
});
