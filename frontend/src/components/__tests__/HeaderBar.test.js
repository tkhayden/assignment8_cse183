import {render, screen, fireEvent, waitFor} from '@testing-library/react';
import {act} from 'react-dom/test-utils';
import '@testing-library/jest-dom';
import {rest} from 'msw';
import {setupServer} from 'msw/node';

import HeaderBar from '../App';

const URL = '/login';

const server = setupServer(
  rest.post(URL, (req, res, ctx) => {
    return res(ctx.json({id: 'testid',
      email: 'test@gmail.com', accessToken: '432323232fds'}));
  }),
  rest.get('/v0/listing', (req, res, ctx) => {
    return res(ctx.json([]));
  }),
  rest.get('/v0/Category', (req, res, ctx) => {
    return res(ctx.json([]));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());
beforeEach(() => {
  window.history.pushState({}, 'Home', '/' );
});


describe('Desktop View', () => {
  beforeAll(()=> {
    act(() => {
      global.innerWidth = 1100;
      global.dispatchEvent(new Event('resize'));
    });
  });

  test('Desktop version has quick log-in', async () => {
    render(<HeaderBar/>);
    screen.getByLabelText('email quick log-in');
    screen.getByLabelText('password quick log-in');
  });
  test('Log out button/user email not visible when not logged in', async () => {
    render(<HeaderBar/>);
    const logout = screen.queryByLabelText('log-out button');
    const email = screen.queryByLabelText('users email');
    expect(logout).toBe(null);
    expect(email).toBe(null);
  });
  test('Valid Log in', async () => {
    render(<HeaderBar/>);
    fireEvent.change(screen.getByLabelText('email quick log-in')
      .querySelector('input'), {
      target: {
        value: 'password',
      },
    });
    fireEvent.change(screen.getByLabelText('password quick log-in')
      .querySelector('input'), {
      target: {
        value: 'test@gmail.com',
      },
    });
    act(() => {
      fireEvent.click(screen.getByLabelText('log-in submit button'));
    });
    await waitFor(() => screen.getByText('test@gmail.com'));
    await waitFor(() => screen.getByLabelText('log-out button'));
  });
  test('Log out', async () => {
    render(<HeaderBar/>);
    act(() => {
      fireEvent.click(screen.getByLabelText('log-out button'));
    });
    await waitFor(() => screen.getByLabelText('email quick log-in'));
    await waitFor(() => screen.getByLabelText('password quick log-in'));
  });
  test('Invalid Log in takes you to login page', async () => {
    server.use(
      rest.post(URL, (req, res, ctx) => {
        return res(ctx.status(401));
      }),
    );
    render(<HeaderBar/>);
    fireEvent.change(screen.getByLabelText('email quick log-in')
      .querySelector('input'), {
      target: {
        value: 'password',
      },
    });
    fireEvent.change(screen.getByLabelText('password quick log-in')
      .querySelector('input'), {
      target: {
        value: 'test@gmail.com',
      },
    });
    act(() => {
      fireEvent.click(screen.getByLabelText('log-in submit button'));
    });
    await waitFor(() => screen.getByText('Email Address'));
    await waitFor(() =>screen.getByText('Password'));
    await waitFor(() =>screen.getByText('Sign in'));
    await waitFor(() => screen.getByText('Don\'t have an account? Sign Up'));
  });
});

describe('Mobile View', () => {
  beforeAll(()=> {
    act(() => {
      global.innerWidth = 500;
      global.dispatchEvent(new Event('resize'));
    });
  });
  test('Mobile version. No quick log-in', async () => {
    render(<HeaderBar/>);
    const email = screen.queryByLabelText('email quick log-in');
    const password = screen.queryByLabelText('password quick log-in');
    expect(email).toBe(null);
    expect(password).toBe(null);
  });
  test('Log-in button directs user to log-in page', async () => {
    render(<HeaderBar/>);
    fireEvent.click(screen.getByLabelText('log-in button'));
    await waitFor(() => screen.getByText('Email Address'));
    await waitFor(() =>screen.getByText('Password'));
    await waitFor(() =>screen.getByText('Sign in'));
    await waitFor(() => screen.getByText('Don\'t have an account? Sign Up'));
  });
});
