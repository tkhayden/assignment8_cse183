import {render, screen, fireEvent, waitFor} from '@testing-library/react';
import {
  listingData, categoriesData, subCategoryData} from './MockData';
import '@testing-library/jest-dom';
import {act} from 'react-dom/test-utils';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import Categories from '../Categories';
import {CategoryUpdateContext} from '../providers/CategoryProvider';

const URL = '/v0/category';

const server = setupServer(
  rest.get(URL, (req, res, ctx) => {
    return res(ctx.json(categoriesData));
  }),
  rest.get('/v0/listing', (req, res, ctx) => {
    return res(ctx.json(listingData));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());
describe('Desktop View Testing', () => {
  beforeAll(()=> {
    act(() => {
      global.innerWidth = 1100;
      global.dispatchEvent(new Event('resize'));
    });
  });
  test('Category text  are visible', async () => {
    const m = jest.fn();

    render(<Categories dimensions={1000} setOpen={m}
      setListings={m} open={true} />);

    await waitFor(() => screen.getByText('Vehicles'));
    await waitFor(() => screen.getByText('Property Rentals'));
  });
  test('Category icons are visible', async () => {
    const m = jest.fn();
    render(<Categories dimensions={1000} setOpen={m}
      setListings={m} open={true} />);

    await waitFor(() => screen.getByAltText('Vehicles'));
    await waitFor(() => screen.getByAltText('Property Rentals'));
  });
  test('Category icon click', async () => {
    const setList = jest.fn();
    const setopen = jest.fn();
    const mockUpdate = jest.fn();
    server.use(
      rest.get(`${URL}/3fc8800d-56bd-41bb-b103-026c0e1453da`,
        (req, res, ctx) => {
          return res(ctx.json(subCategoryData[1]));
        }));
    act(() => {
      render(
        <CategoryUpdateContext.Provider value={mockUpdate}>
          <Categories dimensions={1000} setOpen={setopen}
            setListings={setList} open={true} />)
        </CategoryUpdateContext.Provider>,
      );
    });
    await waitFor(() => fireEvent.click(screen.getByText('Vehicles')));
    await waitFor(() => expect(setList.mock.calls.length).toBe(1));
    await waitFor(() => expect(setopen.mock.calls.length).toBe(2));
    await waitFor(() => expect(mockUpdate.mock.calls.length).toBe(1));
  });
  test('Category fetch 500 error', async () => {
    const m = jest.fn();
    server.use(
      rest.get(URL, (req, res, ctx) => {
        return res(ctx.status(500));
      }),
    );
    render(<Categories dimensions={1000} setOpen={m}
      setListings={m} open={true} />);


    const none = screen.queryByText('Vehicles');
    expect(none).toBe(null);
  });
});

describe('Mobile view testing', () => {
  beforeAll(()=> {
    act(() => {
      global.innerWidth = 400;
      global.dispatchEvent(new Event('resize'));
    });
  });
  test('Category text  is visible', async () => {
    const m = jest.fn();

    render(<Categories dimensions={400} setOpen={m}
      setListings={m} open={true} />);

    await waitFor(() => screen.getByText('Vehicles'));
    await waitFor(() => screen.getByText('Property Rentals'));
  });
});
