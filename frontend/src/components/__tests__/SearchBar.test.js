import {render, screen, waitFor} from '@testing-library/react';
import {allListingsURL, listingData, searchQueryListingData} from './MockData';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import {act} from 'react-dom/test-utils';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import SearchBar from '../SearchBar';


const server = setupServer(
  rest.get(allListingsURL, (req, res, ctx) => {
    const search = req.url.searchParams.get('search');
    if (search) {
      return res(ctx.json(searchQueryListingData));
    } else {
      return res(ctx.json(listingData));
    }
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('Mobile View', () => {
  beforeAll(()=> {
    act(() => {
      global.innerWidth = 500;
      global.dispatchEvent(new Event('resize'));
    });
  });

  test('Search Bar Visible For Mobile', async () => {
    const listings = (json) => {
      return json;
    };
    render(<SearchBar setListings={listings}/>);
    screen.getByLabelText('search listings');
  });

  test('Typing into Search Bar', async () => {
    const listings = (json) => {
      return json;
    };
    render(<SearchBar setListings={listings}/>);
    await waitFor(() => screen.getByLabelText('search listings'));
    const searchBar = screen.getByLabelText('search listings')
      .querySelector('input');
    await act(async () =>
      await waitFor(() => userEvent.type(searchBar, 'Ford')));
    expect(searchBar.value).toBe('Ford');
  });

  test('Listings Get Filtered After Search', async () => {
    const listings = (json) => {
      return json;
    };
    render(<SearchBar setListings={listings}/>);
    await waitFor(() => screen.getByLabelText('search listings'));
    const searchBar = screen.getByLabelText('search listings')
      .querySelector('input');
    userEvent.type(searchBar, 'F');
    await new Promise((r) => setTimeout(r, 2500));
  });

  test('Handles Server Error', async () => {
    server.use(
      rest.get(allListingsURL, (req, res, ctx) => {
        return res(ctx.status(500));
      }),
    );
    const listings = (json) => {
      return json;
    };
    render(<SearchBar setListings={listings}/>);
    await waitFor(() => screen.getByLabelText('search listings'));
    const searchBar = screen.getByLabelText('search listings')
      .querySelector('input');

    await act(async () =>
      await waitFor(() => userEvent.type(searchBar, 'F{enter}')));
    try {
      // NO listings should appear on error
      await waitFor(() => screen.getByLabelText('listing'));
    } catch (e) {
      // eslint-disable-next-line jest/no-conditional-expect
      expect(e.message).not.toBeNull();
    }
  });

  test('Pressing Enter Button on Search', async () => {
    const listings = (json) => {
      return json;
    };
    render(<SearchBar setListings={listings}/>);
    await waitFor(() => screen.getByLabelText('search listings'));
    const searchBar = screen.getByLabelText('search listings')
      .querySelector('input');

    await act(async () =>
      await waitFor(() => userEvent.type(searchBar, '   {enter}')));
  });
});
