import {render, screen, fireEvent, waitFor} from '@testing-library/react';
import {act} from 'react-dom/test-utils';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import LoginPage from '../LoginPage';
import {UserUpdateContext} from '../providers/UserProvider';
import {createMemoryHistory} from 'history';
import {Router} from 'react-router-dom';
import '@testing-library/jest-dom';

const URL = '/login';

const server = setupServer(
  rest.post(URL, (req, res, ctx) => {
    return res(ctx.json({id: '2b03b5f6-36af-443a-a8b4-e870c76eba56',
      email: 'test@gmail.com', accessToken: '432323232fds'}));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('Valid Log in', async () => {
  const mock = jest.fn();
  const history = createMemoryHistory();
  render(
    <Router history={history}>
      <UserUpdateContext.Provider value={mock}>
        <LoginPage/>
      </UserUpdateContext.Provider>
    </Router>,
  );
  fireEvent.change(screen.getByLabelText('Email Address')
    .querySelector('input'), {
    target: {
      value: 'test@gmail.com',
    },
  });
  fireEvent.change(screen.getByLabelText('Password')
    .querySelector('input'), {
    target: {
      value: 'password',
    },
  });
  await act(async () => {
    await waitFor(() => fireEvent.click(screen.getByLabelText('Login Button')));
  });
  await waitFor(() => expect(mock).toHaveBeenCalledTimes(1));
//   await new Promise((r) => setTimeout(r, 2500));
});


test('Invalid Log in with Non Existing Email', async () => {
  server.use(
    rest.post('/login', (req, res, ctx) => {
      return res(ctx.status(404));
    }),
  );

  const mock = jest.fn();
  const history = createMemoryHistory();
  render(
    <Router history={history}>
      <UserUpdateContext.Provider value={mock}>
        <LoginPage/>
      </UserUpdateContext.Provider>
    </Router>,
  );

  await waitFor(() => {
    fireEvent.change(screen.getByLabelText('Email Address')
      .querySelector('input'), {
      target: {
        value: 'notRealUser@gmail.com',
      },
    });
  });
  fireEvent.change(screen.getByLabelText('Password')
    .querySelector('input'), {
    target: {
      value: 'fakepassword',
    },
  });
  await act(async () => {
    await waitFor(() => fireEvent.click(screen.getByLabelText('Login Button')));
  });
  await waitFor(() => screen.getByText('Incorrect Email or Password'));
  await waitFor(() => expect(mock).toHaveBeenCalledTimes(0));
});
