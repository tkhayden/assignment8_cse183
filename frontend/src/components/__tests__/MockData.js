export const allListingsURL = '/v0/listing';
export const getCategoriesURL = '/v0/category';
export const listingByIdURL =
  '/v0/listing/e47327e8-61ab-430b-a852-b1e6a92c03ac';
export const invalidListingIdURL =
  '/v0/listing/e47327e8-61ab-430b-a852-b1e6a92c03ab';

export const listingData = [
  {
    'id': 'e47327e8-61ab-430b-a852-b1e6a92c03ac',
    'category_id': 'a4383321-84ff-4b6e-8f57-86fd4ae7d9b9',
    'member_id': '7b917fe4-a164-414c-a80b-1b84bb2a4dd3',
    'listing': {
      'price': '1,200',
      'title': '2012 Ford Ranger',
      'images': [
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2020-ford-ranger-106-1571685769.jpg?crop=0.840xw:0.840xh;0.0657xw,0.110xh&resize=240:*',
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2020-ford-ranger-101-1571685768.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2021-ford-ranger-tremor-106-leadstatic-1612842282.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2021-ford-ranger-tremor-349-leadaction-1612842298.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
      ],
      'replies': [
        {
          'content': 'Can you do $150?',
          'userName': 'Barry Gallanders',
          'datePosted': '2020-01-19T01:27:34Z',
        },
        {
          'content': 'Looks great!',
          'userName': 'Rod Meecher',
          'datePosted': '2020-01-31T02:45:39Z',
        },
      ],
      'location': {
        'city': 'Santa Cruz',
        'state': 'CA',
        'country': 'Unites States',
      },
      'date_posted': '2020-01-17T01:27:34Z',
      'description': 'test description',
    },
  },
  {
    'id': 'c6787a4a-4452-464c-8b9c-d851a0a1285e',
    'category_id': '3fc8800d-56bd-41bb-b103-026c0e1453da',
    'member_id': '40113ca4-4857-48d6-a1f3-eecab58a67c6',
    'listing': {
      'price': '42,000',
      'title': '2018 Subaru WRX STI',
      'images': [
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2020-subaru-wrx-sti-mmp-1-1568320026.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
        'https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/media/51/2015-subaru-wrx-inline2-photo-566933-s-original.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
        'http://o.aolcdn.com/hss/storage/adam/3e7ceb911290bc86c3c23cce3a6273e8/lead6-2015-subaru-wrx-fd.jpg',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQI8-trTdo4xQuoycNDZ_ABttF1BvcWK_oXKaM82DO2usqzh64llsaCxN2oCh6zIneDUlw&usqp=CAU',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9hSpO7JR0O4LbOe0ccFP_fMb8W-8f2KF3eZTXK_N3UkzMLnHcYrPI4tGXrVR9SNkErF4&usqp=CAU',
        'https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/media/51/2015-subaru-wrx-inline1-photo-566856-s-original.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
        'https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/14q1/562747/2015-subaru-wrx-sedan-manual-test-review-car-and-driver-photo-566711-s-original.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
        'https://pictures.dealer.com/r/ryesubarusoa/1914/fb108294f06e60730f55c0a294b9c041x.jpg?impolicy=resize&w=200',
      ],
      'replies': [
        {
          'content': 'Are you willing to go any lower?',
          'userName': 'Kris Steffan',
          'datePosted': '2020-01-21T01:34:59Z',
        },
      ],
      'location': {
        'city': 'Santa Cruz',
        'state': 'CA',
        'country': 'Unites States',
      },
      'date_posted': '2020-01-17T01:27:34Z',
      'description': 'test description',
    },
  },
  {
    'id': '8465534f-ad7e-4fc1-a33e-d2b2dcc1d617',
    'category_id': 'ed5a6dc4-eabf-4412-83ba-bdacb37de451',
    'member_id': '7a453931-2308-45c6-b3b4-e7084ad83a93',
    'listing': {
      'price': '9,200',
      'title': '2010 Toyota Prius',
      'images': [
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2020-toyota-prius-mmp-1-1565732347.jpg?crop=0.891xw:1.00xh;0.0353xw,0&resize=240:*',
        'https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/15q2/657948/2015-toyota-prius-review-car-and-driver-photo-660322-s-original.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
        'https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/11q4/424154/2012-toyota-prius-plug-in-hybrid-photos-and-info-news-car-and-driver-photo-433599-s-original.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
      ],
      'replies': [],
      'location': {
        'city': 'Santa Cruz',
        'state': 'CA',
        'country': 'Unites States',
      },
      'date_posted': '2020-01-17T01:27:34Z',
      'description': 'test description',
    },
  },
  {
    'id': '416d7372-1efc-4a80-b59c-f7477f158429',
    'category_id': '115b60b4-414f-4a11-b7d7-58705aaf7be0',
    'member_id': '7a453931-2308-45c6-b3b4-e7084ad83a93',
    'listing': {
      'price': '1,000',
      'title': '2 Bed 2 Bath Apartment',
      'images': [
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2-1-1634825177.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/small-apartment-design-06-1500582812.jpg?crop=0.840xw:0.840xh;0.0657xw,0.0xh&resize=240:*',
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/small-apartment-design-07-1500582839.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
      ],
      'replies': [],
      'location': {
        'city': 'Santa Cruz',
        'state': 'CA',
        'country': 'Unites States',
      },
      'date_posted': '2020-01-17T01:27:34Z',
      'description': 'test description',
    },
  },
  {
    'id': '18505546-c2c4-4500-b778-78a6289a4e27',
    'category_id': '77786811-1098-4252-a52e-8e3903113151',
    'member_id': '5b92ca7e-6504-4fd5-afc2-3b00f139234e',
    'listing': {
      'price': '1,900',
      'title': '3 Bed 1 Bath House',
      'images': [
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/scream-airbnb-02-exterior-credit-helynn-ospina-1632926760.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
      ],
      'replies': [],
      'location': {
        'city': 'El Sobrante',
        'state': 'CA',
        'country': 'Unites States',
      },
      'date_posted': '2020-01-17T01:27:34Z',
      'description': 'test description',
    },
  },
  {
    'id': 'e47327e8-61ab-430b-a852-b1e6a92c03ab',
    'listing': {
      'price': 'FREE',
      'title': 'FAKE ITEM',
      'images': [],
      'replies': [],
      'location': {
        'city': 'El Sobrante',
        'state': 'CA',
        'country': 'Unites States',
      },
    },
  },
];

export const searchQueryListingData = [
  {
    'id': 'e47327e8-61ab-430b-a852-b1e6a92c03ac',
    'category_id': 'a4383321-84ff-4b6e-8f57-86fd4ae7d9b9',
    'member_id': '7b917fe4-a164-414c-a80b-1b84bb2a4dd3',
    'listing': {
      'price': '1,200',
      'title': '2012 Ford Ranger',
      'images': [
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2020-ford-ranger-106-1571685769.jpg?crop=0.840xw:0.840xh;0.0657xw,0.110xh&resize=240:*',
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2020-ford-ranger-101-1571685768.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2021-ford-ranger-tremor-106-leadstatic-1612842282.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2021-ford-ranger-tremor-349-leadaction-1612842298.jpg?crop=0.840xw:0.840xh;0.0657xw,0.124xh&resize=240:*',
      ],
      'replies': [],
      'location': {
        'city': 'Santa Cruz',
        'state': 'CA',
        'country': 'Unites States',
      },
      'date_posted': '2020-01-17T01:27:34Z',
      'description': 'test description',
    },
  },
  {
    'id': '6a169e35-9fad-444c-a950-796c1c3155bf',
    'category_id': 'ed5a6dc4-eabf-4412-83ba-bdacb37de451',
    'member_id': '5b92ca7e-6504-4fd5-afc2-3b00f139234e',
    'listing': {
      'price': '6,500',
      'title': '1998 Ford Mustang',
      'images': [
        'https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/images/media/267321/1996-ford-mustang-gt-archived-instrumented-test-review-car-and-driver-photo-559707-s-original.jpg?crop=0.832xw:0.912xh;0,0&resize=240:*',
      ],
      'replies': [],
      'location': {
        'city': 'Dallas',
        'state': 'TX',
        'country': 'Unites States',
      },
      'date_posted': '2020-01-17T01:27:34Z',
      'description': 'test description',
    },
  },
  {
    'id': '5033610d-c73b-49fd-bbc8-9e1798307ba5',
    'category_id': 'a4383321-84ff-4b6e-8f57-86fd4ae7d9b9',
    'member_id': '3cd6c9dc-a941-4702-8476-16c71c6244f1',
    'listing': {
      'price': '32,000',
      'title': '2015 Ford F1-50',
      'images': [
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2021-ford-f-150-rendering-1586190703.jpg?crop=0.870xw:0.912xh;0,0.106xh&resize=240:*',
      ],
      'replies': [],
      'location': {
        'city': 'Watsonville',
        'state': 'CA',
        'country': 'Unites States',
      },
      'date_posted': '2020-01-17T01:27:34Z',
      'description': 'test description',
    },
  },
];


export const categoriesData = [
  {
    'id': '3fc8800d-56bd-41bb-b103-026c0e1453da',
    'category': {
      'name': 'Vehicles',
      'subcategories': [
        'a4383321-84ff-4b6e-8f57-86fd4ae7d9b9',
        'ed5a6dc4-eabf-4412-83ba-bdacb37de451',
      ],
      'associated_filters': [],
      'icon': 'https://img.icons8.com/material-outlined/24/000000/suv--v1.png',
    },
  },
  {
    'id': 'be2141dd-e48a-4964-9675-d8d942b009d3',
    'category': {
      'name': 'Property Rentals',
      'associated_filters': [],
      'subcategories': [
        '115b60b4-414f-4a11-b7d7-58705aaf7be0',
        '77786811-1098-4252-a52e-8e3903113151'],
      'icon': 'https://img.icons8.com/material-outlined/24/000000/sell-property--v1.png'},
  },
];

export const subCategoryData = [
  {
    'id': '77786811-1098-4252-a52e-8e3903113151',
    'parent_id': 'be2141dd-e48a-4964-9675-d8d942b009d3',
    'categories': {
      'name': 'House',
      'associated_filters': [],
      'subcategories': [],
    },
  },
  {
    'id': 'a4383321-84ff-4b6e-8f57-86fd4ae7d9b9',
    'parent_id': '3fc8800d-56bd-41bb-b103-026c0e1453da',
    'categories': {
      'name': 'Trucks',
      'associated_filters': [],
      'subcategories': [],
    },
  },
];

export const categoryIDdata = [
  {
    'id': '3fc8800d-56bd-41bb-b103-026c0e1453da',
    'category': 'Vehicles',
    'subcategories': [
      {
        'id': 'a4383321-84ff-4b6e-8f57-86fd4ae7d9b9',
        'category': {
          'name': 'Trucks',
          'associated_filters': [],
        },
      },
      {
        'id': 'ed5a6dc4-eabf-4412-83ba-bdacb37de451',
        'category': {
          'name': 'Cars',
          'associated_filters': [],
        },
      },
    ],
    'icon': 'https://img.icons8.com/material-outlined/24/000000/suv--v1.png',
  },
  {
    'id': 'ed5a6dc4-eabf-4412-83ba-bdacb37de451',
    'category': 'Cars',
    'subcategories': [],
  },
];
