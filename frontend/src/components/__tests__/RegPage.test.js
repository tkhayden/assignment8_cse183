import {render, screen, fireEvent, waitFor} from '@testing-library/react';
import {act} from 'react-dom/test-utils';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import RegPage from '../RegPage';
import {UserUpdateContext} from '../providers/UserProvider';
import {createMemoryHistory} from 'history';
import {Router} from 'react-router-dom';
import '@testing-library/jest-dom';


const server = setupServer(
  rest.post('/register', (req, res, ctx) => {
    return res(ctx.json({id: 'testid',
      email: 'test@gmail.com', accessToken: '432323232fds'}));
  }),
  rest.post('/login', (req, res, ctx) => {
    return res(ctx.json({id: 'testid',
      email: 'test@gmail.com', accessToken: '432323232fds'}));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('Valid Sign Up', async () => {
  const mock = jest.fn();
  const history = createMemoryHistory();
  render(
    <Router history={history}>
      <UserUpdateContext.Provider value={mock}>
        <RegPage/>
      </UserUpdateContext.Provider>
    </Router>,
  );
  fireEvent.change(screen.getByLabelText('Email Address')
    .querySelector('input'), {
    target: {
      value: 'test@gmail.com',
    },
  });
  fireEvent.change(screen.getByLabelText('Password')
    .querySelector('input'), {
    target: {
      value: 'password',
    },
  });
  await act(async () => {
    await waitFor(() =>
      fireEvent.click(screen.getByLabelText('Sign Up Button')));
  });
  await waitFor(() => expect(mock).toHaveBeenCalledTimes(1));

//   await new Promise((r) => setTimeout(r, 2500));
});

test('Invalid Sign Up with Existing Email', async () => {
  server.use(
    rest.post('/register', (req, res, ctx) => {
      return res(ctx.status(409));
    }),
  );

  const mock = jest.fn();
  const history = createMemoryHistory();
  render(
    <Router history={history}>
      <UserUpdateContext.Provider value={mock}>
        <RegPage/>
      </UserUpdateContext.Provider>
    </Router>,
  );

  await act(async () => {
    await waitFor(() => fireEvent.click(
      screen.getByLabelText('Sign Up Button')));
  });

  await waitFor(() =>
    screen.getByText('User with submitted Email already exists',
      {exact: false},
    ));
  await waitFor(() => expect(mock).toHaveBeenCalledTimes(0));
});

test('Handle Server Error Attempting to Log In After Sign Up', async () => {
  server.use(
    rest.post('/login', (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );

  const mock = jest.fn();
  const history = createMemoryHistory();
  render(
    <Router history={history}>
      <UserUpdateContext.Provider value={mock}>
        <RegPage/>
      </UserUpdateContext.Provider>
    </Router>,
  );

  fireEvent.change(screen.getByLabelText('Email Address')
    .querySelector('input'), {
    target: {
      value: 'test@gmail.com',
    },
  });
  fireEvent.change(screen.getByLabelText('Password')
    .querySelector('input'), {
    target: {
      value: 'password',
    },
  });

  await act(async () => {
    await waitFor(() => fireEvent.click(
      screen.getByLabelText('Sign Up Button')));
  });

  await waitFor(() =>
    screen.getByText('Unexpected Error Occured',
      {exact: false},
    ));
  await waitFor(() => expect(mock).toHaveBeenCalledTimes(0));
});
